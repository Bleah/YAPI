[ "Georg Simon Ohm in a Can" pr nl
  "Usage:" pr nl
  "number ,v --> tell it how many volts" pr nl
  "number ,a --> tell it how many amps" pr nl
  ",o -> ohms; ,w -> watts; ,h -> horsepower" pr nl
  ",p -> amps/phase if dealing with 3phase" pr nl
  "also: ,k = kilo; ,M = mega; ,m = milli" pr nl
  "      ,g = giga; ,u = micro; ,n = nano" pr nl
  "Example: 300 ,v 200 ,m ,a" pr nl
  "==> it calculates how many watts, etc. there are" pr nl
  "if there are 300volts and 200mA" pr nl
  ",om = show this helptext again" pr nl
] >om

"Georg Simon Ohm in a Can. Type ,om for help." pr nl

(Ohms    Volts   Amps    Watts    Horse   Amps-per-phase)
 0.0 >O  0.0 >V  0.0 >A  0.0 >W   0.0 >H  0.0 >P

(kilo, milli) [1000 mul] >k  [1000 div] >m
(Mega, micro) [1000000 mul] >M  [1000000 div] >u
(giga, nano)  [1000000000 mul] >g  [1000000000 div] >n

[=O ,go] >o  (enter ohms and calc other stuff)
[=V ,go] >v  (enter volts)
[=A ,go] >a  (amps)
[=W ,go] >w  (watts)
[=H ,go] >h  (horsepower)
[=P ,go] >p  (amps/Phase if 3-phase)

['V 'A div =O] >VAO      (given volts and amps, calc ohms)
['V 'A mul =W] >VAW      (volts,amps->watts)
['V 'O div =A] >VOA      (volts,ohms->amps)
['W 'V div =A] >WVA      (watts,volts->amps)
['A 'O mul =V] >AOV      (amps,ohms->volts)
['W 'A div =V] >WAV      (watts,amps->volts)
['W 'O div sqrt =A] >WOA (watts,ohms->amps)
['W 'O mul sqrt =V] >WOV (watts,ohms->volts)
['A 3 sqrt div =P] >AP   (amps DC/1phase->amps/phase 3phase)
['P 3 sqrt mul =A] >PA   (amps 3phase back to singlephase/DC)
['W 746 div =H] >WH      (watts->horsepower)
['H 746 mul =W] >HW      (horsepower->watts)

[0 eq swap t] >z               (run thing if val == 0)
[0 eq swap f] >nz               (run thing if val != 0)
[0 eq swap 0 eq or swap f] >both (run thing if 2 nonzero vals)

[ 'PA 'P ,nz (if there's amps/phase, turn that into just amps)
  'HW 'H ,nz (if there's horsepower, turn that into watts)
  [,VAO ,VAW ,sho] 'V 'A ,both
  [,VOA ,VAW ,sho] 'V 'O ,both
  [,WVA ,VAO ,sho] 'W 'V ,both
  [,AOV ,VAW ,sho] 'A 'O ,both
  [,WAV ,VAO ,sho] 'W 'A ,both
  [,WOA ,WOV ,sho] 'W 'O ,both
] >go

[ 'WH 'H ,z (first, conv watts to HP if HP=0)
  'AP 'P ,z (and convert amps to A/P if A/P=0)
  'V pr " Volts" pr nl
  'A pr " Amps(if DC or single-phase AC)" pr nl
  'O pr " Ohms" pr nl
  'W pr " Watts" pr nl
  'H pr " HP" pr nl
  'P pr " Amps/phase(if 3-phase AC)" pr nl
  0 =V  0 =A  0 =O  0 =W  0 =P  0 =H
] >sho
