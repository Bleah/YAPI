// to do? have var search look outside of obj if not found inside?
// to do? if you call ,myObj it looks for "main" inside of myObj to run?

// BUG?  brk might go goofy when in a named subrt
// BUG!  fmt segfaults if bad fmt$ sent to springf(C bug)

// NOTE: timer doesn't run while waiting for user input,
//       and apparently not while pausing either
// ALSO: you can't + or - from stack refs, becaues that
// would conflict with + or - number entry.

// simple types like numbers are direct, everything else is allocated
// so before that, make the list '# stuff take ranges?
// to-do:
// much later, if even possible:
// shared-library use,ls(dir),shell,rs-232,network,graphics??

#include <stdlib.h>
#include <stdio.h>
#include <math.h>   // Also must include -lm on compile command line !!!
#include <string.h>
#include <unistd.h> // for usleep and chdir
#include <time.h>   // for timer and now

//n #include <sys/socket.h> // for network  (need an ifdef or something)
//n #include <arpa/inet.h>  // for network

#define uint unsigned int
#define maxrand 2147483647
#define objMarker "þ" // crazy char no one would normally use

#define si sizeof(int)

#define usrFn void // just to better identify direct-from-go funcs
#define usrCmd void // just to better identify direct-from-go cmds

//-------------------------------------<native command list>
char* cFuncs[]={ // These are the functions a script can call
      "fmt", //  0 kind of like sprintf
       "pr", //  1 print any value onscreen
        "s", //  2 show contents of stack
    "loops", //  3 run list a number of times
      "inc", //  4 increment an integer
        "m", //  5 show total memory allocation
       "cp", //  6 make a copy of a value
        "c", //  7 clear the stack
        "v", //  8 list variables
      "dup", //  9 duplicate top stack item
     "swap", // 10 swap top two stack items
    "debug", // 11 show debug info
  "nodebug", // 12 don't show debug info
      "run", // 13 load and run a script
       "nl", // 14 new line
       "sp", // 15 space
       ">#", // 16 insert item(s) into list
       "<#", // 17 cut item(s) from list
       "'#", // 18 get val from list
       ";#", // 19 delete list item
       "=#", // 20 replace val in list   (maybe need =$, etc?)
     "drop", // 21 discard top stackitem
      "new", // 22 delete all variables
       "eq", // 23 check if 2 values equal
       "tf", // 24 an if-then-else
   "doloop", // 25 a do-loop
       "lt", // 26 check if a < b
       "gt", // 27 check if a > b
      "gte", // 28 greater than or equal
      "lte", // 29 less than or equal
      "neq", // 30 not equal
     "file", // 31 open a file
    "rdstr", // 32 get chars from file
    "range", // 33 make list: range of numbers
   "reduce", // 34 run what's in a list, but leave results in a list
      "map", // 35 run a subrt on each listitem, make a list of results
      "mul", // 36 multiply 2 numbers
     "xmul", // 37 vector cross-product
     "dmul", // 38 vector dot product
      "sub", // 39 subtractino
      "div", // 40 division
      "neg", // 41 negate a number
      "inv", // 42 invert (x = 1/x)
      "obj", // 43 turn list into obect
     "quit", // 44 (or just "q")
        "t", // 45 do list if condition is true
        "f", // 46 if false
      "dim", // 47 an array of all the same value 
    "bload", // 48 load a file as a binary string
     "each", // 49 do same function on each item in a list
      "dec", // 50 decrement an integer
      "pow", // 51 to the power of
      "sqr", // 52 square
     "sqrt", // 53 square root
     "loop", // 54 a regular (might run 0-times) while/until loop
 "list2str", // 55 make a string from a bloc
     "find", // 56 find pos of one string in another(0 if not found)
     "type", // 57 say what type a value is
     "pmul", // 58 polynomial multiplication
      "len", // 59 length of list or string (ynot obj?)
     "ands", // 60 (logic)
      "ors", // 61 ...
    "nands", // 62 ...
     "nors", // 63 ...
     "xors", // 64 ...
    "xnors", // 65 ...
      "mod", // 66 modulo, but that also takes 2 ints
     "nots", // 67 ...
      "and", // 68 now some boolean
       "or", // 69 ...
     "nand", // 70 ...
      "nor", // 71 ...
      "xor", // 72 ...
     "xnor", // 73 ...
      "not", // 74 ...
  "bin2int", // 75 string -> integer
  "int2bin", // 76 integer -> string
 "str2list", // 77 string -> list of integers
  "reverse", // 78 ...
       "pi", // 79 trig constant
    "input", // 80 user input
      "eof", // 81 end-of-file
     "rdln", // 82 read line from file
    "cases", // 83 switch/case
    "getio", // 84 reserve hardware ports
   "freeio", // 85 free them
     "outb", // 86 output to hardware ports
     "inpb", // 87 read port
    "split", // 88 split on cr/lf
        "h", // 89 show some help
      "shl", // 90 shift integer bits to left
      "shr", // 91 to right
      "arg", // 92 N arg -> bails subrt if not N stkitem
     "args", // 93 same thing
  "seconds", // 94 a short delay
    "timer", // 95 time since prog started
    "floor", // 96
    "round", // 97
     "ceil", // 98
     "irnd", // 99
      "rnd", // 100
  "str2num", // 101 string to real number
      "sys", // 102
       "cd", // 103
      "INT", // 104 require an integer
     "REAL", // 105 require a real#
      "STR", // 106
     "LIST", // 107
      "OBJ", // 108
     "FILE", // 109
      "NUM", // 110
     "eval", // 111
    "bsave", // 112
     "seek", // 113
    "rdint", // 114
      "leg", // 115 lt,eq,gt
     "int?", // 116
    "real?", // 117
     "str?", // 118
    "list?", // 119
     "obj?", // 120
    "file?", // 121
     "num?", // 122
    "items", // 123
      "add", // 124
   "substr", // 125
    "slice", // 126
   "splice", // 127
      "sin", // 128
      "cos", // 129
      "tan", // 130
     "asin", // 131
     "acos", // 132
     "atan", // 133
      "deg", // 134
      "rad", // 135
     "trim", // 136
 "findcase", // 137
       "nz", // 138 not zero
     "chop", // 139
      "pos", // 140
     "skip", // 141
    "wrstr", // 142
    "wrint", // 143
   "select", // 144
   "ltlend", // 145
   "bigend", // 146
        "d", // 147 disk dir "ls"
      "cls", // 148
    "floop", // 149 kind of like a "for" loop ...
     "here", // 150
    "while", // 151
    "until", // 152
      "now", // 153
        "@", // 154
     "stop", // 155
      "brk", // 156
      "psb", // 157 print string as a binary
       "ld", // 158
       "do", // 159
     "help", // 160
     "pull", // 161
       "+#", // 162
       "-#", // 163
       "*#", // 164
       "/#", // 165
       "&#", // 166
       "|#", // 167
       "^#", // 168
       "%#", // 169 
      "++#", // 170
      "--#", // 171
        "z", // 172 IS it zero? (int, real, strlen, etc)
   "listen", // 173 listen to HTTP port
     "hear", // 174 accept a connection and get the query
    "reply", // 175 reply in HTML
   "midstr", // 176 absolute-position vertion of substr
   "urldec", // 177 decode the percent crap in URLs
     "mime", // 178 set mimestr
       "lz", // 179 is it less than zero?
       "gz", // 180 is it greater than zero?
"allsplice", // 181 splice an entire list of stuff
     "home", // 182 recall where YAPI was started
 "yapiinfo", // 183 some info about the YAPI internals
      "log", // 184 natural log
     "sort", // 185
NULL}; // The order they're in has to match the numbered cases in "go"
#define numFuncs 185

char homeDir[1024]={0}; // where YAPI is started & yapi.txt should be


// some networking stuff
//n #define inBufSize 32768 // max allowable query from web browser
//n #define maxBacklog 10   // the maximum number of waiting connections
//n int servSock=0;         // the server socket
//n int surfer;             // the browser that's connecting to this
//n char inBuf[inBufSize];  // Input buffer
//n struct sockaddr_in adrPort={AF_INET,0,0}; // addr and port (AF_INET=2)
//n int drip; // for detecting PIPE errors and stuff...
//n int listeningTo=0;      // what port?
//n int heard=0;            // HAS a connection been accepted?
//n char mimeStr[70]="text/html"; // can be changed



void putStr(char *s){int n;for(n=0;s[n];n++)putchar(s[n]);}//and no nl

void putStrQ(char *s){
  int n;
  putchar('"');
  for(n=0;s[n];n++)putchar(s[n]);
  putchar('"');
}//needs to not show control/esc chars unless told to

char dbug = 0;  // if 1, debug messages show
char err = 0;   // if 1, YAPI traces back to console
#define OK err<1
#define BAD err>0
short brack = 0;// level of bracket-nest for tokenizing lists

#define dbg(c) if(dbug)putchar(c)
#define dot dbg('.')
#define bail(S) {printf("\n");putStr(S);err=1;return;}
#define gripe(S) {printf("\n");putStr(S);err=1;}
#define zbail(S) {printf("\n");putStr(S);err=1;goto z;}
#define crash(S) {printf("\n");puts(S);exit(1);}



// ======== A "malloc" that keeps count of allocated memory ========
#define allocs 2048 // if you're running out of alloc, increase this
//#define allocs 65535
void* alloc_adrs[allocs+1];
int alloc_amts[allocs+1];   // 2 gig per alloc is max with int
char alloc_types[allocs+1];
int allocated;              // for 64bit, this would be LLI

void maloc_init(){
  int i;
  for(i=1; i<=allocs; i++){alloc_adrs[i] = NULL; alloc_amts[i] = 0;}
  allocated = 0;
};

void* maloc(int amt, char typ){ // limited to 2gig per alloc
  int n; void* v;
  if(dbug)printf("+%d",amt);
  v = (void*)malloc(amt);
  if(v == NULL)crash("Failed to allocate memory");
  allocated += amt;
  for(n=1; n<=allocs; n++) if(alloc_amts[n] == 0){
    alloc_amts[n] = amt;
    alloc_adrs[n] = v;
    alloc_types[n] = typ;
    dot;
    return v;}
  crash("maloc isn't set to allocate that many blocs");
};

// showMem far below

void* realoc(void *p, int amt){
  int n;
  dbg('*');
  if(p == NULL)crash("tried to re-allocate a NULL pointer");
  for(n=1;n<=allocs;n++)if(alloc_adrs[n]==p){
    allocated = allocated + amt - alloc_amts[n];
    alloc_adrs[n]=(void*)realloc(p,amt);
    if(alloc_adrs[n]==NULL)crash("Failed to re-allocate memory");
    alloc_amts[n] = amt;
    return alloc_adrs[n];}
  crash("realoc crashed trying to reallocate a non-allocated pointer");
};

void* freee(void* p){
  int n;
  dbg('-');
  if(p==NULL)crash("Tried to free a NULL pointer");
  for(n=1;n<=allocs;n++)if(alloc_adrs[n]==p){
    allocated -= alloc_amts[n];
    alloc_adrs[n] = NULL;
    alloc_amts[n] = 0;
    free(p);
    return NULL;}
  printf("Tried to free unallocated memory at %p\n",p);exit(1);
};
#define fre(p) p=freee(p)



//-------------------------------<Types>
#define var union named_var // just so I don't have to type union each time
#define val struct typed_value
#define obj var* // object is array of var, just as list is array of val
#define list val* // yup, a list is just an array of values
#define filD struct file_descr
#define uType union multi_types

filD{int Usage; FILE* Handle; char Mode; char Name[256];};
//socD{int Usage; socket* Handle; }bleah, maybe just do ONE socket;http

uType{int I; double R; list L; char *S; obj O; filD *F;};

val{             // struct for a typed value
  char vT;       // TYPE: one of {uTypes}
//char fluff[7]; // there are 7 unused bytes here for future? use
  uType vD;      // DATA: the data of whatever type, be it number or pointer
};

var{           // a union of 2 things
  struct{      // part 1 of the union (objects)
    int alloc; // for objects (memory)
    int len;   // how many vars in this obj
    int usage;  // usage count of obj
  }; // this first part has data in the FIRST element of the object
  struct{         // part 2 of the union (plain named vars)
    val vV;       // the val for this var   !AFTER val is def!
    char vartype; // for strict
    char vN [30]; // varname goes 0 to 29, so #29=\0
  }; // this last section is what the rest of them have
};

// the choices for someVal.vT
enum uTypes{anInt,aReal,aList,aMarker,aCfunc,anObj,aStr,aCmd,aFile};
// and "NUM" is typenum -1, so "lenient" has to be -2, I suppose...
// @#^& tricky to get all these definitions in the right ORDER
#define aNum -1
#define anyType -2
int STRICT=anyType; // lenient
int LENIENT=0; // timeout for strict

#define fHandle vD.F->Handle

val cmd2val(char *s);
val sub2vals(val a, val b);
val int2val(int i);

#define zilch int2val(0)
#define retZ return int2val(0)

// some forwards
usrFn cpVal();
void useVal(val V);
void dropVal(val v);

void dropFile(filD* fv);
void useList(list L);
void dropList(list L);   // or should THESE
void useStr(char* str);
void dropStr(char* str); // all be val instead of type-specific?

//val add2vals(val a, val b);

usrFn doFile(int runit);
obj go(val subrt, obj vo);
#define ogo(s,v) v=go(s,v)
val detect(char* DS);
void tokenize(char* tLine);

void showValMin(val V);
void showVal(val v);

val mkCfunc(char* s);
// valStr and valStrChr were here



//---------------------------<objects>
#define objLen(O) O[0].len     // how many variables are in this obj
#define valObjLen(V) V.vD.O[0].len
#define objAlloc(O) O[0].alloc // how many can it hold w/o realloc
#define objUsage(O) O[0].usage // how many times the obj is pointed-to

obj newObj(int len){
  obj nu;
  nu = (obj)maloc(sizeof(var)*(len+1),anObj);//"len+1": 1st is header
  objAlloc(nu) = len; // keep track of how many vars it has room for
  objLen(nu) = 0;     // it starts out empty
  objUsage(nu) = 1;   // a new thing starts out with usage = 1
  return nu;
}//newObj: makes a blank object of a predetermined alloc-length

void killObj(obj o){
  int x;
  for(x=objLen(o);x>=1;x--)dropVal(o[x].vV);
  fre(o);
}

#define useObj(o) objUsage(o)++

void dropObj(obj o){if(--objUsage(o)==0)killObj(o);}

#define maxGlob 999
obj globs; // the global-variables list is a predefined object
void initGlobs(){globs=newObj(maxGlob);}
void clrVars(){killObj(globs);initGlobs();}

obj sto_new_o( // Tack a new var onto a var list
  obj o,      // The var list to append
  char* N,   // Name of new var
  val V     // Value of new var
){
  if(++objLen(o)>objAlloc(o)){// oops, it'll go beyond what's alloc
//  nu = (obj)maloc(sizeof(var)*(len+1),anObj);from newobj
//  printf("Expanding object realloc from %d ",objAlloc(o));
    objAlloc(o)=objLen(o)+3; // (+9) is a bit of memory overcommit
    o=(obj)realoc(o,sizeof(var)*(objAlloc(o)+1)); // RE-allocate
//  printf("to %d (not recommended)\n",objAlloc(o));
    }
  o[objLen(o)].vV=V;   // Tack val onto the END
  o[objLen(o)].vartype=STRICT;
  if(strlen(N)>28)N[29]=0;//truncate so as not to overrun varname alloc
  strcpy(o[objLen(o)].vN,N); // strcpy stops at the null-char(0)
  return o;
}// the realloc part might be buggy if it really has to re-allocate
#define sto_new(ob,nm,vl) ob=sto_new_o(ob,nm,vl)
// sto_new: used by mkVar, fn2obj, objcat.  val >var


obj objCat(obj o1,obj o2){
  obj o3;       // the resulting combined obj
  int comboLen; // the length it will be
  int x;        // count thru o1
  int y;        // count thru 02
  char dupl;    // how many vars did BOTH objects have (duplicates)?
  comboLen=objLen(o1)+objLen(o2); // at least to begin with
  for(x=1;x<=objLen(o1);x++)
    for(y=1;y<=objLen(o2);y++)
      if(!strcmp(o1[x].vN,o2[y].vN))
        comboLen--; // decrement each time a duplicate is found
//now comboLen has the number of vars in o1 and o2, minus the duplicates
  o3=newObj(comboLen); // must make the obj b4 doing anything else to it
//still need to FILL the new obj
  for(x=1;x<=objLen(o1);x++){
    dupl=0;
    for(y=1;y<=objLen(o2);y++)if(!strcmp(o1[x].vN,o2[y].vN))dupl=1;
    if(dupl==0){useVal(o1[x].vV);sto_new(o3,o1[x].vN,o1[x].vV);}
  } // OK, that takes care of vars from o1 that are NOT also in o2
//NOW stuff from o2 can go into o3 without burying predecessors from o1
  for(y=1;y<=objLen(o2);y++)
    {useVal(o2[y].vV);sto_new(o3,o2[y].vN,o2[y].vV);}
  if(dbug){
    printf("obj Len %d\n",objLen(o3));
    printf("alloc8d %ld\n",objAlloc(o3)*sizeof(var));
    for(x=1;x<=objLen(o3);x++){
      putStr(o3[x].vN);putchar(':');
      showVal(o3[x].vV);putchar(' ');
    }
  }
  return o3;
} // usage:  'oldobj 'changes splice >anewobj




//--------------------------------------------------<STACK>
#define maxStk 999
int sp=0;          // that's the stack pointer
val stk[maxStk+1]; // the stack itself

char noStk(int N){return(sp<N);};

//define need1 if(noStk(1))bail("Stack was empty " )
#define needStk(N,S) if(sp<N)bail(S);

val stkpop(){
  if(sp>0)return(stk[sp--]);
  gripe("Needed an item on the stack for ");
  retZ;
}

#define pop stkpop()
int popi(){val v=pop;if(v.vT!=anInt)crash("popInt->!int");return v.vD.I;}
#define popInt popi()
double popr(){val v=pop;if(v.vT!=aReal)crash("popReal->!real");return v.vD.R;}
#define popReal popr()
list popl(){val v=pop;if(v.vT!=aList)crash("popList->!list");return v.vD.L;}
#define popList popl()
obj popo(){val v=pop;if(v.vT!=anObj)crash("popObj->!obj");return v.vD.O;}
#define popObj popo();
char* pops(){val v=pop;if(v.vT!=aStr)crash("popStr->!str");return v.vD.S;}
#define popStr pops()
#define string char*  // just to show when it's a string with usage/len
filD* popf(){val v=pop;if(v.vT!=aFile)crash("popFile->!file");return v.vD.F;}
#define popFile popf();

void push(val P){
  if(sp>=maxStk){
    dropVal(P);
    do dropVal(pop);while(sp>20);
    bail("Stack overflow; dropping all but 20 ");
  }
  stk[++sp]=P;
}

#define pushInt(i) push(int2val(i))
#define pushReal(f) push(real2val(f))
#define pushNum(f) push(num2val(f))
#define pushObj(o) push(obj2val(o))
#define pushMkr(m) push(marker2val(m))
#define pushList(L) push(list2val(L))
#define pushCf(s) push(mkCfunc(s))
#define pushQs(s) push(Qs2val(s))
#define pushStr(s) push(str2val(s))
//-------------------------------------------------</STACK>



void showVarNames(obj o){
  int x;
  for(x=objLen(o);x>=1;x--)printf(" %s ",o[x].vN);
}//used only by showval.  Shows names of all vars in obj o


// ======== Convenience: make typed values ========
val int2val(int i){val v;v.vT=anInt;v.vD.I=i;return v;}
val real2val(double f){val v;v.vT=aReal;v.vD.R=f;return v;}
val marker2val(int m){val v;v.vT=aMarker;v.vD.I=m;return v;}
val num2val(double f){
  if(f==(int)f)return int2val((int)f);else return real2val(f);
}
val list2val(list L){val v;v.vT=aList;v.vD.L=L;return v;}
val file2val(filD* F){val v;v.vT=aFile;v.vD.F=F;return v;}
#define pushFile(f) push(file2val(f))
val obj2val(obj o){val v;v.vT=anObj;v.vD.O=o;return v;}
//  list2val & obj2val ASSUME the list/obj has already been allocated

#define isInt(v) v.vT==anInt
#define isReal(v) v.vT==aReal
#define isStr(v) v.vT==aStr
#define isList(v) v.vT==aList
#define isObj(v) v.vT==anObj
#define isFile(v) v.vT==aFile
#define isNum(N) ((N.vT==anInt)||(N.vT==aReal))

#define notInt(v) v.vT!=anInt
#define notReal(v) v.vT!=aReal
#define notStr(v) v.vT!=aStr
#define notList(v) v.vT!=aList
#define notObj(v) v.vT!=anObj
#define notFile(v) v.vT!=aFile
#define notNum(v) ((v.vT!=anInt)&&(v.vT!=aReal))

#define needInt(v) if(notInt(v))zbail("That aint an integer ");
#define needReal(v) if(notReal(v))zbail("That aint a real# ");
#define needStr(v) if(notStr(v))zbail("That aint a string ");
#define needList(v) if(notList(v))zbail("That aint a list ");
#define needObj(v) if(notObj(v))zbail("That aint an object ");
#define needFile(v) if(notFile(v))zbail("That aint a file ");
#define needNum(v) if(notNum(v))zbail("That aint a number ");

#define hazInt(n) stk[sp-n+1].vT==anInt
#define hazReal(n) stk[sp-n+1].vT==aReal
#define hazNum(n) ((stk[sp-n+1].vT==aReal)||(stk[sp-n+1].vT==anInt))
#define hazStr(n) stk[sp-n+1].vT==aStr
#define hazList(n) stk[sp-n+1].vT==aList
#define hazFile(n) stk[sp-n+1].vT==aFile
#define hazObj(n) stk[sp-n+1].vT==anObj

#define noInt(n) stk[sp-n+1].vT!=anInt
#define noReal(n) stk[sp-n+1].vT!=aReal
#define noNum(n) ((stk[sp-n+1].vT!=aReal)&&(stk[sp-n+1].vT!=anInt))
#define noStr(n) stk[sp-n+1].vT!=aStr
#define noList(n) stk[sp-n+1].vT!=aList
#define noFile(n) stk[sp-n+1].vT!=aFile
#define noObj(n) stk[sp-n+1].vT!=anObj

#define strS(S) &S[si*2] // string part of a counted char*
#define strChr(S,N) S[si*2+N] // a char in a string
#define valStr(SV) &SV.vD.S[si*2] //the STRING part of a val.string
#define valStrChr(SV,N) SV.vD.S[si*2+N] // a char in a string val
int strLen(char* vS){int *L;L=(int*)&vS[si]; return *L;}
int setStrLen(char* vS,int sL){memcpy(&vS[si],&sL,si);}
#define valStrLen(V) strLen(V.vD.S)
#define setValStrLen(V,L) setStrLen(V.vD.S,L)
void useStrOnce(string s){int u=1; memcpy(&s[0],&u,si);}
int strUsage(string x){int *u; u=(int*)&x[0]; return *u;}

void useStr(string s){
  int u;
  memcpy(&u,&s[0],si); // copy data from string into an int
  u++;                 // increment the int
  memcpy(&s[0],&u,si); // copy data back into string
//this just increments the part of the string where usage count is stored
}//dropStr is still somewhere else I gues.

string newStr(int L){
  string S;
  S=(char*)maloc(L+si*2+1,aStr); //the +1 is for nulterm
  useStrOnce(S);
  return(S);
}

string newCmd(int L){
  string S;
  S=(char*)maloc(L+si*2+1,aCmd); //the +1 is for nulterm
  useStrOnce(S);
  return(S);
}

//  create new string or cmd and put it already in a val
val newStrVal(int L){val vS; vS.vT=aStr; vS.vD.S=newStr(L);return vS;}
val newCmdVal(int L){val vC; vC.vT=aCmd; vC.vD.S=newCmd(L);return vC;}

val str2val(string S){val vS;vS.vT=aStr;vS.vD.S=S;return vS;}//counted-str

val Qs2val(char* qs){ // a simple C quoted string
  val sV; // val to put the string into
  int sL; // its length
  sL=strlen(qs); // get the char* length
  sV=newStrVal(sL);
  strcpy(valStr(sV),qs); // copy the data
  setValStrLen(sV,sL);   // set the length
  return sV;
}

val cmd2val(char* qs){ // ===== cmd-string =====
  val cV; // val to put it in
  int cL; // its length
  cL=strlen(qs);
  cV=newCmdVal(cL);
  strcpy(valStr(cV),qs);
  setValStrLen(cV,cL);
  return cV;
}//cmd2val: make a val of type "aCmd" out of char*


/*void putStrValNoWeirdChars(val s){
  int n; char c;
  for(n=0;n<valStrLen(s);n++){
    c=valStrChr(s,n);
    if((c>31)&&(c<127))putchar(c);
    else if((c==13)||(c==10))putchar(c);
  }
}*/

void putStrVal(val s){
  int n;for(n=0;n<valStrLen(s);n++)putchar(valStrChr(s,n));}

// ======== Using Lists ========
#define listLen(L) L[2].vD.I
#define valListLen(V) V.vD.L[2].vD.I

int listNdx(list L, int n){ // trap out out-of-bounds indices
  if(n<1){
    gripe("List index below 1 is not allowed ");//crash("AAAGH");
    return 3;} // 3 is where the first actual list item is
  if(n>listLen(L)){ // return the first item and complain
    gripe("List index was beyond end of list ");
    return listLen(L)+2;} // return the last item and complain
  return n+2; // the actual list item index, after the reserved stuff
}//NOTE: sets err if out-of-range (via "gripe")

#define listUsage(L) L[0].vD.I
#define valListUsage(V) V.vD.L[0].vD.I
#define listAllocLen(L) L[1].vD.I
#define valListAllocLen(V) V.vD.L[1].vD.I
#define listMemSize(L) (L[1].vD.I+3)*sizeof(val)
#define listItem(L,n) L[listNdx(L,n)]
#define valListVal(V,n) V.vD.L[listNdx(V.vD.L,n)]
#define valListType(V,n) V.vD.L[listNdx(V.vD.L,n)].vT
#define listItemType(L,n) L[listNdx(L,n)].vT
#define listItemData(L,n) L[listNdx(L,n)].vD
#define listItemList(L,n) L[listNdx(L,n)].vD.L
#define listItemInt(L,n) L[listNdx(L,n)].vD.I
#define listItemStr(L,n) L[listNdx(L,n)].vD.S
#define listItemStrS(L,n) &L[listNdx(L,n)].vD.S[si*2]
#define listItemReal(L,n) L[listNdx(L,n)].vD.R
#define forListNdx(L,n) for(n=1;n<=listLen(L);n++)

void useList(list L){dbg('L');listUsage(L)++;dot;}
void useFile(filD* fv){dbg('F');fv->Usage++;dot;}

void useVal(val V){
  switch(V.vT){
    case aCmd:  // (non-Cfunc cmd is stored as a string)
    case aStr:  useStr(V.vD.S);break;
    case aList: useList(V.vD.L);break;
    case anObj: useObj(V.vD.O);break;
    case aFile: useFile(V.vD.F);break;
//  default is do nothing since the rest are nonallocated
  }
}

list newList(int len){ //allocate mem and make a list waiting to be filled
  list L;
  ++len; //overcommit +1
// 3 reserved(usage,alloc,length)
  L=(list)maloc((3+len)*sizeof(val),aList);
  listUsage(L)=1; listAllocLen(L)=len; listLen(L)=0; return L;
}

list XappendList(list L, val V){
  if(listLen(L)>=listAllocLen(L)){   // beyond end of allocated for list
//  printf("Expanding list alloc from %d ",listAllocLen(L));
    listAllocLen(L)=(10+listLen(L)); // some mem overcommit...
//  printf("to %d\n",listAllocLen(L));
    L=(list)realoc(L,(3+listAllocLen(L))*sizeof(val));} // make room
  useVal(V);
  listItem(L,++listLen(L))=V;                 // and store the item
  return L;
}
#define appendList(L,V) L=XappendList(L,V)

void dropList(list L){// dropping a list (since lists are allocated)
  int n;
  if(--listUsage(L)<1){ // only if nothing is using it anymore
    forListNdx(L,n)dropVal(listItem(L,n));//Kill'em all
    fre(L); // and the list they were stored in, too
  }
}

int findListStartOnStk(){ // how far down in the stack did a list begin?
  int sp2;
  for(sp2=sp;sp2>0;sp2--)
   if((stk[sp2].vT==aMarker)&&(stk[sp2].vD.I==aList))//found it
    return sp2; // and there's its index on the stack
  puts("couldn't find list-start marker on the stack");
  err=1; brack=0; return 0;
}

void fillListFromStk(list L){ // Fills a newly-allocated list to its length
  int n;                      // using items taken from the stack
  if(sp<listLen(L))bail("? not enuf onstack to fill this list");
  listLen(L)=listAllocLen(L)-1; // fill it
  forListNdx(L,n)listItem(L,listLen(L)-n+1)=pop; // so as not backwards
  pop;                        // the marker, that is; and discard it
}
// --since fillListFromStk is only used by mkListFromStk, COMBINE FUNCS
// ---- and here's the make-a-list function a user-script would call ----
list mkListFromStk(){       // Create a new list (allocating mem),
  list L;                   // and fill it using items
  int n;                    // taken from the stack.
  n = findListStartOnStk(); // That's where the list-start marker is
  L = newList(sp-n);      // stackpointer-n is the length of the new list
  fillListFromStk(L);       // pop enough values to fill the list
  return L;
}

#define pushListFromStk pushList(mkListFromStk())

// ======== Some Math ========



val mul2vals(val a, val b){ // NESTED VECTORS ARE NOT ALLOWED
  char aT; char bT; list L; int n;
  aT = a.vT; bT = b.vT;
  if(aT==anInt){
    if(bT==anInt)return int2val(a.vD.I*b.vD.I);
    if(bT==aReal)return num2val(a.vD.I*b.vD.R);}
  if(aT==aReal){
    if(bT==anInt)return num2val(a.vD.R*b.vD.I);
    if(bT==aReal)return num2val(a.vD.R*b.vD.R);}
  if((bT==aList)&&((aT==anInt)||(aT==aReal))){
    forListNdx(b.vD.L,n)
      if(notNum(valListVal(b,n)))
        zbail("Only numbers and vectors allowed ");
    L=newList(valListLen(b));
    forListNdx(b.vD.L,n)appendList(L,mul2vals(a,valListVal(b,n)));
    dropList(b.vD.L); return list2val(L);}
  if((aT==aList)&&((bT==anInt)||(bT==aReal))){
    forListNdx(a.vD.L,n)
      if(notNum(valListVal(a,n)))
        zbail("Only numbers and vectors allowed ");
    L=newList(valListLen(a));
    forListNdx(a.vD.L,n)appendList(L,mul2vals(b,valListVal(a,n)));
    dropList(a.vD.L);return list2val(L);}
  if((aT==aList)&&(bT==aList)){
    puts("For multiplying two vectors, you need to specify whether");
    puts("you want the dot-product(dmul), the cross-product(xmul),");
    putStr("or if you're trying to multiply 2 polynomials(pmul). ");
    goto z;}
  putStr("Only numbers and vectors allowed ");
z:err=1;dropVal(a);dropVal(b);retZ;}//might just let map do listmul...



val add2vals(val a, val b){//NESTED VECTORS ARE NOT ALLOWED
  char aT; char bT; list L; int n;
  aT = a.vT; bT = b.vT;
  if(aT==anInt){
    if(bT==anInt)return int2val(a.vD.I+b.vD.I);
    if(bT==aReal)return real2val(a.vD.I+b.vD.R);}
  if(aT==aReal){
    if(bT==anInt)return real2val(a.vD.R+b.vD.I);
    if(bT==aReal)return num2val(a.vD.R+b.vD.R);}
  if((aT==aList)&&(bT==aList)){ //  adding 2 vectors
    if(valListLen(a)==valListLen(b)){
      forListNdx(a.vD.L,n)
        if((notNum(valListVal(a,n)))||notNum(valListVal(b,n)))
        zbail("Only numbers and vectors allowed ");
      L = newList(valListLen(b));
      forListNdx(a.vD.L,n)
        appendList(L,add2vals(valListVal(a,n),valListVal(b,n)));
      dropList(a.vD.L);dropList(b.vD.L);return list2val(L);}
    else zbail("The two vectors need to be the same length ");
  }
  gripe("Only numbers and vectors allowed ");
z:dropVal(a);dropVal(b); retZ;}

val sub2vals(val b, val a){//NESTED VECTORS ARE NOT ALLOWED
  char aT; char bT; list L; int n;
  aT=a.vT;bT=b.vT;
  if(aT==anInt){
    if(bT==anInt)return int2val(b.vD.I-a.vD.I);
    if(bT==aReal)return real2val(b.vD.R-a.vD.I);}
  if(aT==aReal){
    if(bT==anInt)return real2val(b.vD.I-a.vD.R);
    if(bT==aReal)return real2val(b.vD.R-a.vD.R);}
  if((aT==aList)&&(bT==aList)){ //  subtract vector from vector
    if(valListLen(a)==valListLen(b)){
      forListNdx(a.vD.L,n)
        if((notNum(valListVal(a,n)))||notNum(valListVal(b,n)))
        zbail("Only numbers and vectors allowed ");
      L = newList(valListLen(b));
      forListNdx(a.vD.L,n)
        appendList(L,sub2vals(valListVal(b,n),valListVal(a,n)));
      dropList(a.vD.L);dropList(b.vD.L);return list2val(L);}
    else zbail("The two vectors need to be the same length ");
  }
  gripe("Only numbers and vectors allowed ");
z:dropVal(a);dropVal(b);retZ;}

int fileLen(val V){
  long int tmp; long int flen;
  tmp=ftell(V.fHandle);          // record where it is now
  fseek(V.fHandle,0L,SEEK_END);  // go to END of file
  flen=ftell(V.fHandle);         // get file pos at end
  fseek(V.fHandle,tmp,SEEK_SET); // go back to where it was
  return flen;                   // report what pos was at end.
}

short sholistlev = 0; // for keeping track of showing nested lists

void shoStrMin(string S){ // show a string, truncated for the screen
  int n;
  if(strLen(S)<40){putStrQ(strS(S));return;}//no need to trunc if < 31
  putchar(34);
  for(n=0;n<=30;n++)putchar(strChr(S,n));//show the 1st 30 chars
  putStr("...");
}

void shoListMin(list L){ // show list, truncated for the screen
  int LS;    // length of list L
  int n;     // counting
  char more; // would there have been more to show?
  more=0;
  LS=listLen(L);
  if(LS>20){LS=20;more=1;} // yup, too big to not truncate it
  putchar('[');sholistlev++;
  for(n=1;n<=LS;n++){printf(" ");showValMin(listItem(L,n));}
  sholistlev--;
  if(more)putStr(" ... ");else putStr(" ]");
}

void showValMin(val V){ // Display value, truncated for the screen
  if(isList(V))shoListMin(V.vD.L);
  else if(isStr(V))shoStrMin(V.vD.S);
  else showVal(V);
}

void showVal(val v){
  int n; int y; list L;
  L=v.vD.L; // just in case
  switch(v.vT){
    case anInt: printf("%d",v.vD.I);break;
    case aReal: printf("%lf",v.vD.R);break;
    case aStr:
      if(dbug)printf("String (usage %d) ",strUsage(v.vD.S));
      putStrQ(valStr(v));
      break;
    case aCmd:
      if(dbug)printf("Command (usage %d) ",strUsage(v.vD.S));
      putStr(valStr(v));
      break;
    case aList:
      putchar('[');sholistlev++;
      forListNdx(L,n){
      printf(" "); //was  for(y=1;y<=sholistlev;y++)printf(" ");
        showValMin(listItem(L,n));}
      sholistlev--;
//    for(y=1;y<=sholistlev;y++)printf(" ");
      putStr(" ]");
      if(dbug)printf(" usage %d\n",listUsage(L));
      break;
    case anObj: putchar('{');showVarNames(v.vD.O);printf("}");break;
    case aMarker: printf("(?)Marker %d\n",v.vD.I);break;
    case aCfunc: printf("%s",cFuncs[v.vD.I]);break;
    case aFile:
      putStr("file ");
      switch(v.vD.F->Mode){
        case 'r': putStr("opened read-only");break;
        case 'w': putStr("created for write");break;
        case 'a': putStr("opened append-only");break;
        case 'R': putStr("opened for read/write");break;
        case 'W': putStr("created for read/write");break;
        case 'A': putStr("opened for read/append");break;}
      printf(": %s",v.vD.F->Name);
      break;
    default: printf("showVal: unknown vTyp %d\n",v.vT);
  }
}//showVal: displays a value regardless of its type

//-------<drop>--------
void dropStr(char* str){ // a COUNTED string
  int u;                 // usage count
  memcpy(&u,&str[0],si); // get u from stringdata
  u--;                   // decrement it
  if(u<1)fre(str);       // de-allocate if not in use somewhere
  else memcpy(&str[0],&u,si); // if it is, put new u back in strdata
}

void dropFile(filD* fv){
  if(--fv->Usage > 0)return; // still in use somewhere so leave it open
  fclose(fv->Handle);       // or if not, close the file
  fre(fv);                 // and de-allocate its filehandle
  if(dbug)printf("File %s closed\n",fv->Name);
}

void dropVal(val v){ // only does anything if it's an allocated type
  switch(v.vT){
    case aCmd:  // (same as a string)   // these 4 data types have
    case aStr:  dropStr(v.vD.S);break;  // stuff allocated inside
    case aList: dropList(v.vD.L);break; // them, so they have to be
    case anObj: dropObj(v.vD.O);break;  // dealt with recursively
    case aFile: dropFile(v.vD.F);break;
//  no default, because if it isn't one of those 4, just do nothing.
//  (since int and real are by-val inside the val and not alloc)
  }
}

usrFn udrop(){
  needStk(1,"Nothing on the stack TO drop ");
  dropVal(pop);
}

void pushNewObjFromVars(obj vars){//depends on STACK and VARS
  int x;    // count
  obj o;    // the object to create
  int oLen; // its length
  for(x=objLen(vars);x>=1;x--)          // start here & find the marker
    if(strcmp(vars[x].vN,objMarker)==0){// found it
      oLen=objLen(vars)-x;              // how many variables to move?
      o=newObj(oLen);                 // make the obj to contain them
      objLen(o)=oLen;                 // set the object's length
      memcpy(&o[1],&vars[x+1],(sizeof(var)*oLen)); // copy the stuff
      objLen(vars)=x-1;                 // delete vars from source object
      pushObj(o);
      return;
    }
  // and if not, do nothing until the marker IS found...
  puts("Can't close an object without opening one first ");bail("on ");
}//ONLY used by fn2obj so combine(later)

char nzp(val N){ // sep2024 bugfix (memleak)
  int X;
  if(isInt(N))X=((N.vD.I>0)-(N.vD.I<0));
  else if(isReal(N))X=((N.vD.R>0.0)-(N.vD.R<0.0));
  else if(isList(N))X=(valListLen(N)>0);
  else if(isStr(N))X=(valStrLen(N)>0);
  else if(isObj(N))X=(valObjLen(N)>0);
  else if(isFile(N))X=(fileLen(N)>0);
  else X=0;
  dropVal(N);return X;
}
#define neg(N) nzp(N)==-1
#define zero(N) nzp(N)==0
#define pos(N) nzp(N)==1

int vsame(val a, val b){
  if(a.vT!=b.vT)return(0);
  if(isInt(a))return(a.vD.I==b.vD.I);
  if(isReal(a))return(a.vD.R==b.vD.R);
  if(isStr(a))return(!strcmp(valStr(a),valStr(b)));
  gripe("can only compare int, real, string ");return(0);
}

// vComp compares magnitude.  vsame just gives eq/neq.

int vComp(){// maybe vcomp should use vsame instead of doing its own
  int c; double f;  
  val a; val b;
  if(noStk(2))zbail("Need 2 values to compare ");
  a=pop; b=pop;
  if(a.vT==b.vT)switch(a.vT){ // need int/real compare also
    case aCfunc:
    case anInt: c=a.vD.I-b.vD.I;return(c>0)-(c<0);
    case aReal: f=a.vD.R-b.vD.R;return(f>0.0)-(f<0.0);
    case aStr:
      c=strcmp(valStr(a),valStr(b));dropVal(a);dropVal(b);
      return(c>0)-(c<0);
//  case aList:  ?? how to determine anything but equal/not-equal ??  
//  case anObj:  etc...
    default: dropVal(a);dropVal(b);zbail("dunno HOW to compare THAT ");
  }else{
    if((isInt(a)&&(isReal(b)))){f=a.vD.I-b.vD.R;return(f>0.0)-(f<0.0);}
    if((isReal(a)&&(isInt(b)))){f=a.vD.R-b.vD.I;return(f>0.0)-(f<0.0);}
    dropVal(a);dropVal(b);return 2;//was zbail("Can't compare different types ");
  }
z:return 0;
}//NEEDS better type-converting and listcompare

//------------------------------<absolute hardware-port# I/O>
extern int ioperm(unsigned long from, unsigned long num, int turn_on);
#define outb(p, v) __asm__ ("outb %b0, %w1" : : "a" (v), "d" (p)) // to port
#define inb2(p, v) __asm__ ("inb %w1, %b0" : "=a" (v) : "d" (p)) // from port
char inb(int p){
  char V;
  inb2(p, V);
  return V%256;
} // wrapper. makes a funct.

char portsok[1024];

void initportsok(){memset(portsok,0,1024);}//be sure to call this at start

void rsleep(double T){ // REAL# sleep/usleep combined
  int S; int U;
  S=(int)T;
  U=(int)((T-S)*1000000);
  if(S)sleep(S);
  if(U)usleep(U);
}//ONLY used in "seconds" so combine

//-----------------<these 3 functions find offsets>--------------
int findVarInStk(char* N){//Is N a number and within bounds of stk?
  int I;
  I=atoi(N);
  if((I<1)||(I>sp))return 0;//since there IS NO stack item zero
  return sp+1-I;
}

int findVarInObj(obj O, char* N){//Is N a name to be found in O?
  int I;
  for(I=objLen(O);I>=1;I--)if(strcmp(O[I].vN,N)==0)return I;
  return 0;
//or, return findVarInObj(O's-parent-obj, N)?
}

int findListVar(obj O, char* N){//Does N start with # and is it in O?
  int Iv;
  if(N[0]!='#')return 0; // NOPE, not a list.
  if(strlen(N)<2)return 0; // Just '#' by itself doesn't qualify either.
  Iv=findVarInObj(O, &N[1]); // Chop of the '#' and find THAT.
  if(notList(O[Iv].vV))gripe("That var isn't a list ");
  return Iv;
}
//-----------------------------------------------------------------

val cutItemFromList(val L, int itemNum){
  val got; // val got from list
  int n;   // for counting
  got=valListVal(L,itemNum);
  for(n=itemNum;n<valListLen(L);n++)valListVal(L,n)=valListVal(L,n+1);
  valListLen(L)--;
  return(got);
}

val rclVar(obj vars, char* varName){
  int F;   // Where in the list of vars was the varName found?
  val got; // what val was got from the list?
  int li;  // an index for a list
//if varname is a number, it's actually a stack-item
  if(F=findVarInStk(varName))got=stk[F];
//otherwise it's just a regular varaiable
  else if(F=findVarInObj(vars,varName))got=vars[F].vV;
  else {err=1;putStr("Not found:");retZ;}
  useVal(got);
  return(got);
y:printf("Can't get item #%d from a %d-item list ",li,valListLen(vars[F].vV));
z:retZ;}


val cutVar(obj vars, char* varName){ //o is obj that the var should be in
  int varPos; // Where in vars was varName found?
  int vLL;   // the LENGTH of the list if a list was found
  val V;    // a value to get from vars
  int x;   // counting
//need first to chk for stack reference
if(varPos=findVarInStk(varName)){
  V=stk[varPos];
  for(x=varPos;x<=sp;x++)stk[x]=stk[x+1];sp--;
  return V;}
//if it got here, it aint a list item, maybe a regular var?
  if(x=findVarInObj(vars,varName)){
    V=vars[x].vV; // retrieve the value
//  that'll leave a gap if it wasn't the last one so shift the others down
    if(x<objLen(vars))
      memcpy(&vars[x],&vars[x+1],(sizeof(var)*(objLen(vars)-x)));
    objLen(vars)--;
    return V;}
//  Well, if it got here, it couldn't find it at all
    putStr("Not found:");
//  printf("\nNot found: '%s' ",N);
z:err=1;retZ;} // used by uCutVar AND uDelVar.


//--------------------------<USER fn/cmd, called from GO>--------

usrFn stkDup(){
  val V;
  needStk(1,"Need a value onStack to make a duplicate of ");
  V=stk[sp]; useVal(V); push(V);
}

usrFn stkSwap(){
  val swapping;
  if(sp<2)bail("Need TWO items onstack to swap them ");
  swapping=stk[sp]; stk[sp]=stk[sp-1]; stk[sp-1]=swapping;
}

obj doInObj(obj vars, char* objName){
  int objPos;   // Where was objName found in vars?
  obj doItHere; // The found obj to use for vars when doing "toDo"
  val toDo;     // A subroutine to run using "doItHere" for vars
  objPos=findVarInObj(vars,objName);if(objPos==0)zbail("Object not found:");
  if(notObj(vars[objPos].vV))zbail("@ not an obj ");
  doItHere=vars[objPos].vV.vD.O;
  toDo=pop;
  ogo(toDo,doItHere);
  vars[objPos].vV.vD.O=doItHere;//Store it back
  dropVal(toDo);
z:return vars;}//also must store vars back


usrFn freeallports(){int p;for(p=0;p<=1023;p++)if(portsok[p])ioperm(p,1,0);}



void tsay(int t){
  switch(t){
    case anInt: putStr("an integer");break;
    case aReal: putStr("a real number");break;
    case aStr: putStr("a string");break;
    case aList: putStr("a list");break;
    case anObj: putStr("an object");break;
    case aFile: putStr("a file");break;
    case aMarker: putStr("a marker");break;
    case aCfunc: putStr("a built-in function");break;
    case aCmd: putStr("a variable-using command");break;
    case aNum: putStr("a number (real or integer)");break; // for requiretype
    default: putStr("(something weird IDK)");
  }
}




//STRICT type check
int typeChk(int varType, int valType){
  switch(varType){
    case aStr:
      if(valType != aStr){
      gripe("Tried to put ");tsay(valType);
      putStr(" into a string-only variable ");
      return 1;}else return 0;
      break;
    case anInt:
      if(valType != anInt){
      gripe("Tried to put ");tsay(valType);
      putStr(" into an integer-only variable ");
      return 1;}else return 0;
      break;
    case aReal:
      if(valType != aReal){
      gripe("Tried to put ");tsay(valType);
      putStr(" into a real-number-only variable ");
      return 1;}else return 0;
      break;
    case aNum:
      if((valType != anInt)&&(valType != aReal)){
      gripe("Tried to put ");tsay(valType);
      putStr(" into a numeric-only variable ");
      return 1;}else return 0;
      break;
    case aList:
      if(valType != aList){
      gripe("Tried to put ");tsay(valType);
      putStr(" into a list-only variable ");
      return 1;}else return 0;
      break;
    case anObj:
      if(valType != anObj){
      gripe("Tried to put ");tsay(valType);
      putStr(" into an object-only variable ");
      return 1;}else return 0;
      break;
    case aFile:
      if(valType != aFile){
      gripe("Tried to put ");tsay(valType);
      putStr(" into a file-only variable ");
      return 1;}else return 0;
      break;
    default: return 0; // anytype
  };
  return 0;
};//return: 0 means no type check error
// (uMkVar and uReplVar are the only places that typecheck like this)


usrCmd uReplVar(obj o, char *N){// as in 4.252 =a where a already existed
  val vR;int I;int nL;int LIN;
//maybe add this?
//string vsn; char* vN; // the name of var
//if(N[0])vN=N;
//else{if(noStk(1)||noStr(1))bail("Var name needed ");
//  vsn=popStr;vN=strS(vsn);} // and use vN instead of N
  needStk(1,"Replacement value needed ");
  if(I=findVarInStk(N)){vR=pop;dropVal(stk[I-1]);stk[I-1]=vR;return;}
//if it got here, aint a stackitem, so maybe a variable
  if(I=findVarInObj(o,N)){
    vR=pop;
    if(typeChk(o[I].vartype,vR.vT))zbail("TypeCheck error in ");
    dropVal(o[I].vV);
    o[I].vV=vR;
    return;
  }
//if it got here,not found at all
  bail("Couldn't find variable to reassign ");
  printf("Tried to replace #%d in a %d-item list ",nL,listLen(o[I].vV.vD.L));
z:dropVal(vR);err=1;
}

list insN2Lst(list L, val V2I, int N){int F;
  appendList(L,zilch); // Oh, THIS is why ins b4 NEW last also appends
  for(F=listLen(L);F>N;F--)listItem(L,F)=listItem(L,F-1);
  listItem(L,N)=V2I;return L;
}

//---------------------------------------------------------------------
//-----------------------------<mkVar also Inserts list items>---------
obj uMkVar(obj o, char *N){
  val V; int I; val vI; list L2; int iW;int F;
//  printf("%p->",o);
  if(sp<1){gripe("Nothing onStack to store ");return o;}//depends on sp def

//first, see if N is a stack reference
if(I=findVarInStk(N)){
//printf("Found in %d-high stack at %d\n",sp,I);
  V=pop;
//for(F=1;F<=sp;F++)printf("At %d is %d\n",F,stk[F].vD.I);
  if(I+sp>maxStk)crash("Stack exceeded in uMkVar");
  for(F=sp;F>=I;F--)stk[F+1]=stk[F];sp++;
  stk[I]=V;return o;
}
//if none of these, it must just be a regular var
  if(N[0]=='#')
    {printf("a list called %s wasn't found ",N);err=1;}
    else sto_new(o,N,pop);

z:return o;
}

//---------------------------------------------------------------------



usrCmd doesItExist(obj O, char *N){ // ?varname
  int x;
  int fnd=0; // aint fnd it yet
  for(x=objLen(O);x>=1;x--)if(strcmp(O[x].vN,N)==0){fnd=1;goto f;}
f:pushInt(fnd);}//should this also detect staekitems ??

usrFn uNow(){//automatically puts it in an obj(maybe not best idea?)
  time_t my_time;
  struct tm* timeinfo;
  time(&my_time);
  obj TL;
  timeinfo=localtime(&my_time);
  TL=newObj(6);
  pushInt(timeinfo->tm_sec);uMkVar(TL,"sec");
  pushInt(timeinfo->tm_min);uMkVar(TL,"min");
  pushInt(timeinfo->tm_hour);uMkVar(TL,"hour");
  pushInt(timeinfo->tm_mday);uMkVar(TL,"day");
  pushInt(timeinfo->tm_mon+1);uMkVar(TL,"month");
  pushInt(timeinfo->tm_year+1900);uMkVar(TL,"year");
  pushObj(TL);
}

usrFn usrTimer(){pushInt((int)clock());}//pauses while getting usrinput

usrFn seconds(){ // realNum seconds -> delay
  val D;
  needStk(1,"Delay how MANY seconds? ");D=pop;
//fflush(stdout);// or nl after a pr
  if(isInt(D)&&(D.vD.I>0))sleep(D.vD.I);
  else if(isReal(D)&&(D.vD.R>0))rsleep(D.vD.R);
  else gripe("Needed a positive number (real or int) ");
  dropVal(D);
}

usrFn lsplice(){ // only used by splice, maybe move it there
  list LA; // the two
  list LB; // input lists
  list LC; // output list
  int N;   // span the lists
//  if(noStk(2)||noList(1)||noList(2))bail("Need 2 lists to splice ");
  LB=popList;LA=popList;
  LC=newList(listLen(LA)+listLen(LB));
  forListNdx(LA,N)appendList(LC,listItem(LA,N));
  forListNdx(LB,N)appendList(LC,listItem(LB,N));
  pushList(LC);
z:dropList(LA);dropList(LB);}

usrFn xmul(){ // the CROSS product of two 3d vectors
  list a; list b; list prod; val x; val y; int n;
  if(noStk(2)||noList(1)||noList(2))
    bail("Need 2 vectors for cross-product ");
  b=popList;a=popList;
  if((listLen(a)!=3)||(listLen(b)!=3))
    zbail("The vectors both need to be 3D ");
  for(n=1;n<=3;n++)
    if((notNum(listItem(a,n)))||(notNum(listItem(b,n))))
      zbail("No non-numbers allowed in xmul ");
  prod=newList(3);
  x=mul2vals(listItem(a,2),listItem(b,3));
  y=mul2vals(listItem(a,3),listItem(b,2));
  appendList(prod,sub2vals(x,y));
  x=mul2vals(listItem(a,3),listItem(b,1));
  y=mul2vals(listItem(a,1),listItem(b,3));
  appendList(prod,sub2vals(x,y));
  x=mul2vals(listItem(a,1),listItem(b,2));
  y=mul2vals(listItem(a,2),listItem(b,1));
  appendList(prod,sub2vals(x,y));
  pushList(prod);
z:dropList(a);dropList(b);}

usrFn dmul(){ // the DOT product a b duml -> a1b1+a2b2+...
  list a; list b; val dp; double mSum; int n; int intProd=1;
  if(noStk(2)||noList(1)||noList(2))bail("Need 2 vectors for dot-product ");
  a=popList; b=popList;
  if(listLen(a)!=listLen(b))zbail("Vectors must be same length ");
//BAN NESTED VECTORS HERE
  mSum=0.0;
  forListNdx(a,n){
    dp=mul2vals(listItem(a,n),listItem(b,n));
    if(isInt(dp))mSum+=dp.vD.I;else{intProd=0;mSum+=dp.vD.R;}
  }
  if(intProd)pushInt(floor(mSum));else pushReal(mSum);
z:dropList(a);dropList(b);}//BUG!! CRASHES ON NESTED VECTORS

usrFn pmul(){      // BUG!! CRASHES ON NESTED VECTORS
  list a; list b; // the two polynomials to multiply
  int n; int m;  // and counts to span them
  list prod;    // The resulting polynomial product vector
  int plen;    // Its length
  val x;      // temp var when mult
  if(noStk(2)||noList(1)||noList(2))
    bail("Need 2 vectors for polynomial multiplication ");
  a=popList; b=popList;
//BAN NESTED VECTORS HERE
  plen=listLen(a)+listLen(b)-1;prod=newList(plen);//make room
  for(m=1;m<=plen;m++)appendList(prod,zilch);//fill it with zeros
  forListNdx(a,n)forListNdx(b,m){
    x=mul2vals(listItem(a,n),listItem(b,m));
    listItem(prod,(n+m-1))=add2vals(listItem(prod,(n+m-1)),x);}
  dropList(a);dropList(b);pushList(prod);
}

usrFn len2int(){
  needStk(1,"Need something to find the length of ")
  val V = pop;
  switch(V.vT){
    case aList: pushInt(valListLen(V));break;
    case  aStr: pushInt(valStrLen(V));break;
    case anObj: pushInt(valObjLen(V));break;
    case aFile: pushInt(fileLen(V));break;
    default: pushInt(0);}
  dropVal(V);
}

usrFn real2int(int FRC){ // turn a real into an integer
  double N;  // a real number to round, floor, or ceil
  if(noStk(1)||noNum(1))
    bail("Need a number to make an integer out of ");
  if(isInt(stk[sp]))return; // already int; nothing to do here.
  N=popReal;
  if((N>2147483647)||(N<-2147483648))
    bail("That real number is too big to make into a 32-bit integer ");
  switch(FRC){
    case 1: pushInt(floor(N));break;
    case 2: pushInt(round(N));break;
    case 3: pushInt(ceil(N));break;
  }
}

usrFn irnd(){
  int rmin; int rmax; int oops;
  if(noStk(2)||noInt(1)||noInt(2))
    bail("Need (integer) min and max values ");
  rmax=popInt;rmin=popInt;
  if(rmin>rmax){oops=rmax;rmax=rmin;rmin=oops;}//min > max? swap'em.
  pushInt((rand()%(rmax-rmin+1))+rmin);
}

usrFn rnd(){
  double rmin; double rmax; double oops;
  if(noStk(2)||noReal(1)||noReal(2))
    bail("Need real-number min and max values ");
  rmax=popReal;rmin=popReal;
  if(rmin>rmax){oops=rmin;rmin=rmax;rmax=oops;}
  pushReal((rand()*((rmax-rmin)/maxrand))+rmin);
}

usrFn str2real(){
  string S;    // the string (w/usage/len)
  double R; // a real to turn it into
  int sOK;  // keep trak of whether it converted it ok or not
  if(noStk(1)||noStr(1))bail("Need a string to make into a number ");
  S=popStr;sOK=sscanf(strS(S),"%lf",&R);
  if(sOK)pushReal(R);else pushReal(0.0);
  dropStr(S);
}

usrFn stpow(){ // x to the power of y
  val xV; val yV; char xT; char yT;
  if(noStk(2)||noNum(1)||noNum(2))
    bail("Need 2 numbers if you're going to take one to the power of the other ");
  yV=pop;xV=pop;xT=xV.vT;yT=yV.vT;
  if((yT==aReal)&&(neg(xV)))
    bail("Can't do non-integer powers of negative numbers ");
  if((xT==anInt)&&(yT==anInt))pushInt(pow(xV.vD.I,yV.vD.I));
  else if((xT==anInt)&&(yT==aReal))pushReal(pow(xV.vD.I,yV.vD.R));
  else if((xT==aReal)&&(yT==anInt))pushReal(pow(xV.vD.R,yV.vD.I));
  else if((xT==aReal)&&(yT==aReal))pushReal(pow(xV.vD.R,yV.vD.R));
}

usrFn natLog(){
  double x;
  if(noStk(1)||noNum(1))bail("Need a number to take the natural logarithm of ");
  if(hazInt(1))x=(double)popInt;else x=popReal;
  if(x<=0)bail("Needed a positive number for log ");
  pushReal(log(x));
}

usrFn st_sqrt(){
  if(noStk(1)||noNum(1))bail("Need a number to take square-root of ");
  if(neg(stk[sp]))
    bail("Square root of a negative?  Sorry, no complex numbers yet ");
  pushReal(0.5);stpow();
}

usrFn st_sqr(){
  val x;
  needStk(1,"Need a number to square ");x=pop;
  if(isInt(x))pushInt(x.vD.I*x.vD.I);
  else if(isReal(x))pushNum(x.vD.R*x.vD.R);
//else if(isList(x)){stkDup();pmul();}
  else gripe("That aint a NUMBER ");
  dropVal(x);
}

usrFn add2(){
  val V;
  needStk(2,"Need 2 numbers or 2 equal-length vectors ");
  V=add2vals(pop,pop);//and let add2vals deal with the valtypes
  if(OK)push(V);
}

void incrv(obj vO,char* N){
  int I;
  if((I=findVarInObj(vO,N))==0)bail("Can't find variable to increment: ");
  if(isInt(vO[I].vV))vO[I].vV.vD.I++;
  else bail("Can't directly increment a non-integer ");
}

usrCmd addToVar(obj O,char* N){
  int I;
  if(N[0]=='+'){incrv(O,&N[1]);return;}//just incrementing it
  needStk(1,"Need a number to add to that variable ");
  I=findVarInObj(O,N);if(!I)bail("Variable not found:");
  if(notNum(O[I].vV))bail("Not a number ");
  if(typeChk(O[I].vartype,stk[sp].vT))bail("TypeCheck error in ");
  push(O[I].vV);add2();if(BAD)return;
  O[I].vV=pop;
}

usrFn subt(){
  val V;
  needStk(2,"Need 2 numbers or 2 equal-length vectors ");
  V=sub2vals(pop,pop); if(OK)push(V);
}

void decrv(obj O,char* N){
  int I;
  if((I=findVarInObj(O,N))==0)bail("Can't find variable to decrement: ");
  if(isInt(O[I].vV))O[I].vV.vD.I--;
  else bail("Can't directly decrement a non-integer ");
}

usrCmd subFromVar(obj O,char* N){
  int I;
  if(N[0]=='-'){decrv(O,&N[1]);return;}
  needStk(1,"Need a number to subtract from that variable ");
  I=findVarInObj(O,N);if(!I)bail("Can't find variable to subtract from: ");
  if(notNum(O[I].vV))bail("Not a number ");
  if(typeChk(O[I].vartype,stk[sp].vT))bail("TypeCheck error in ");
  push(O[I].vV);stkSwap();subt();if(BAD)return;
  O[I].vV=pop;
}

usrFn mul2(){
  val V;
  needStk(2,"Need 2 numbers, or a number and a vector ");
  V=mul2vals(pop,pop); if(OK)push(V);
}

usrCmd mulByVar(obj O,char* N){
  int I;
  needStk(1,"Need a number to multiply that variable by ");
  I=findVarInObj(O,N);if(!I)bail("Can't find variable to multiply: ");
  if(notNum(O[I].vV))bail("Not a number ");
  push(O[I].vV);mul2();if(BAD)return;
  if(typeChk(O[I].vartype,stk[sp].vT))bail("TypeCheck error in ");
  O[I].vV=pop;
}

void negn(val* vP){
  int N;
  val V;
  V=*vP;
  if(isInt(V)){vP->vD.I=-V.vD.I;return;}  // integer scalar
  if(isReal(V)){vP->vD.R=-V.vD.R;return;} // real scalar
  if(isList(V)){                          // vector
    forListNdx(V.vD.L,N){
      negn(&valListVal(V,N));
      if(BAD)return;}
    return;}
  err=1;return;//if it got here, not number OR vector
}
usrFn negv(){
  needStk(1,"Need a number(or vector) to negate ");
  if(hazList(1))cpVal(); // don't alter original if vector
  negn(&stk[sp]);if(OK)return;
  dropVal(pop); // wouldn't need to dropval if it was a number
  gripe("Can't negate what aint a number ");
}


usrFn invn(){ // and BTW vectors don't invert
  val v;
  needStk(1,"Need a number to invert(x=1/x) ");v=pop;
  if(notNum(v)){showVal(v);putStr(" aint a number ");err=1;goto z;}
  if(zero(v))zbail("Can't invert zero ");
  if(isInt(v)){push(num2val(1.0/v.vD.I));return;}
  else{push(num2val(1.0/v.vD.R));return;}
z:dropVal(v);} // whatever it was, it might be allocated

usrFn divd(){
  val n; val d; // numerator, denominator
  needStk(2,"Need a numerator and a denominator ");d=pop;n=pop;
  if(notNum(d)){showVal(d);putStr(" aint a number ");err=1;goto z;}
  if(zero(d))zbail("Can't divide by zero ");
  if(isReal(d))d.vD.R=1.0/d.vD.R;
  else{d.vT=aReal;d.vD.R=1.0/d.vD.I;}
  push(mul2vals(d,n));return;//mul2vals can deal with numerator type
z:dropVal(n);dropVal(d);}//z:only drop if not mul&push

usrCmd divByVar(obj O,char* N){
  int I;
  needStk(1,"Need a number to divide that variable by ");
  I=findVarInObj(O,N);if(!I)bail("Can't find variable to divide by: ");
  if(notNum(O[I].vV))bail("Not a number ");
  push(O[I].vV);stkSwap();divd();if(BAD)return;
  if(typeChk(O[I].vartype,stk[sp].vT))bail("TypeCheck error in ");
  O[I].vV=pop;
}

double trigUnits=1;

usrFn trig(int n){
  val V;
  double A;
  if(noStk(1)||noNum(1)){
    gripe("Needed a number to take the ");
    if(n==1)putStr("sine of ");
    if(n==2)putStr("cosine of ");
    if(n==3)putStr("tangent of ");
    if(n==4)putStr("arc-sine of ");
    if(n==5)putStr("arc-cosine of ");
    if(n==6)putStr("arc-tangent of ");
    return;
  }
  V=pop;
  if(isInt(V))A=(double)V.vD.I;else A=V.vD.R;
  switch(n){
    case 1: pushReal(sin(A/trigUnits));break;
    case 2: pushReal(cos(A/trigUnits));break;
    case 3: pushReal(tan(A/trigUnits));break;
    case 4: pushReal(asin(A)*trigUnits);break;
    case 5: pushReal(acos(A)*trigUnits);break;
    case 6: pushReal(atan(A)*trigUnits);break;
  }
}

usrFn stk_inc(){
  if(noStk(1)||noInt(1))bail("Need an integer to increment ");
  stk[sp].vD.I++;
}

usrFn stk_dec(){
  if(noStk(1)||noInt(1))bail("Need an integer to decrement ");
  stk[sp].vD.I--;
}

/* need strstrC, a counted-len version of strstr
int strstrC(string S) a YAPI string
*/

int cisf(char* s1, char* s2, int insen){//case-insensitive find
  int i;
  int L1;
  int L2;
  char* S1; // the copies
  char* S2; // for case-insensitive search
  char* F;  // found?
  if(insen){
    L1=strlen(s1);L2=strlen(s2);       // get the lengths
    S1=malloc(L1+1);S2=malloc(L2+1);   // get mem for copies
    strcpy(S1,s1);strcpy(S2,s2);       // make the copies
    for(i=0;i<=L1;i++)if((S1[i]>='a')&&(S1[i]<='z'))S1[i]&=223; // lcase
    for(i=0;i<=L2;i++)if((S2[i]>='a')&&(S2[i]<='z'))S2[i]&=223; // lcase
    F=strstr(S1,S2);   // TRY to find it
    free(S1);free(S2); // ditch the copies
    return(F==NULL?0:F-S1+1);
  }
  F=strstr(s1,s2);return(F==NULL?0:F-s1+1);
}

usrFn findit(int noCase){// find pos of one string in another if there
   val WTLI;     // what (string or list) to look in
   val WTLF;     // what (string,char,listItem) to look for
   int TPOS;     // index of one thing in the other
   char CC2F[2]; // converted char to find 
   if(noStk(2)){
     putStr("Need a string to look in and a string to find,");
     bail("or a list to look in and an item to find. ");}
   WTLF=pop;WTLI=pop;
   if(isStr(WTLI))goto s;
   if(notList(WTLI))zbail("Needed a string or a list to look in ");
   forListNdx(WTLI.vD.L,TPOS)if(vsame(valListVal(WTLI,TPOS),WTLF))goto r;
   TPOS=0;goto r;//didn't find it
s: CC2F[1]=0;//terminate tmpstr with char just in case
   if(isInt(WTLF)){ // actually looking for ONE char
     CC2F[0]=(char)WTLF.vD.I%256; // cvt int to char
     TPOS=cisf(valStr(WTLI),CC2F,noCase);} // and try to find it
   else if(isStr(WTLF))TPOS=cisf(valStr(WTLI),valStr(WTLF),noCase);//str
   else zbail("Needed a string or char to look for ");//NOPE
r: pushInt(TPOS);
z: dropVal(WTLI);dropVal(WTLF);}

string subStr(string S, int ssPos, int ssLen){
  string ss; // the substring to get
  int SLen;  // length of the input string S
  SLen=strLen(S);ss=newStr(ssLen);setStrLen(ss,ssLen);//gonna fill the str
  if((ssPos>0)&&(ssPos<=SLen)&&(ssLen>0)&&(ssLen+ssPos-1<=SLen)){
    strncpy(strS(ss),strS(S)+ssPos-1,ssLen);strChr(ss,ssLen)=0;}
  else{
    strChr(ss,0)=0;
    gripe("Out-of-range error ");
    printf("trying to get a length(%d) substr at pos(%d)",ssLen,ssPos);
    printf(" from a length(%d) string ", SLen);
  } // null string
  return ss;
}

usrFn uMidStr(){
  string s1; int upos; int ulen;
  if(noStk(3)||noInt(1)||noInt(2)||noStr(3))
    bail("Need a string and the start position and length of a substring to get ");
  ulen=popInt;upos=popInt;s1=popStr;
  if(upos<0)upos=strLen(s1)+upos+1;  // Neg->backwards from end
  if(ulen==0)ulen=strLen(s1)-upos+1; // length(0)=use the ENTIRE REST OF THE STRING
//if(ulen<1)pushQs("");else pushStr(subStr(s1,upos,ulen));
  if(ulen<1)zbail("midstr can't return a negative-length string ");
  pushStr(subStr(s1,upos,ulen));
z:dropStr(s1);
}

usrFn uSubStr(){ // VERY SIMILAR to substr, except begin/end, not pos/len
  string s1; int msBegin; int msEnd; int bla;
  if(noStk(3)||noInt(1)||noInt(2)||noStr(3))
    bail("Need a string and the begin and end positions of a substring to get ");
  msEnd=popInt;msBegin=popInt;s1=popStr;
  if((msBegin==0)||(msEnd==0))zbail("YAPI strings begin with pos 1, not 0 ");
  if(msEnd<0)msEnd=strLen(s1)+msEnd+1; //backwards from end if end$ is neg
  if(msBegin<0)msBegin=strLen(s1)+msBegin+1; //backwards from end if end$ is neg
  if(msEnd<msBegin){bla=msEnd;msEnd=msBegin;msBegin=bla;}
  if(msEnd==msBegin)pushQs("");else pushStr(subStr(s1,msBegin,msEnd-msBegin+1));
z:dropStr(s1);}

usrFn slice(){ // a string, and later a list as well
  string inS;  // input string
  list spL;    // list of split-points
  list opL;    // output list-of-strings
  int x;       // count the listindexes
  int P;       // adding up the slicepoints
  int L;       // length of each slice
  val tmpvS;   // to temporarily store each slice
  if(noStk(2)||noStr(2)||noList(1))
    bail("Need a string and a list of integers for where to slice it ");
  spL=popList;inS=popStr;P=1;
  opL=newList(listLen(spL)+1);//one more outputstr than splitpts
  forListNdx(spL,x){            // each slice point
    L=listItemInt(spL,x);         // get the slicept
    tmpvS=str2val(subStr(inS,P,L));//get the $ between that and prev.pt
    appendList(opL,tmpvS);        // and tack it onto the loaf
    dropVal(tmpvS);            // discard temporary
    if(BAD)goto z;          // don't keep going if couldn't find it
    P+=L;}             // add length to pos for the next slice
  L=strLen(inS)-P+1; // now to just tack on the remainder$
  if(L>0){tmpvS=str2val(subStr(inS,P,L));
    appendList(opL,tmpvS);dropVal(tmpvS);}
z:pushList(opL);dropList(spL);dropStr(inS);
}// slice a list?


//"This is a string" [" "] split --> ["This" "is" "a" "string"]  , or
//"This is a string"  [32] split --> ["This" "is" "a" "string"]
//"http://www.bla.com" ["://" "." "."] split -> ["http" "www" "bla" "com"] or
//"http://www.bla.com" ["://" \. \.] split -> ["http" "www" "bla" "com"]
//(in c, the slash/ might be taken as a special char)
//"This is a string" " " split >a >b >c >d
//"This is a string" 32 split >a >b >c >d
//usrFn split(){
//}



usrFn reverse(){
  val V;   // input value, can be list or string
  int L;   // raw list taken from v if list
  list RL; // raw list for the reversed list
  val RS;  // the reversed string if str
  int I;   // span the list items or string chars
  needStk(1,"Need a list or a string to reverse ");V=pop;
  switch(V.vT){
    case aList: {
      L=valListLen(V);RL=newList(L);
      for(I=L;I>0;I--)appendList(RL,valListVal(V,I));
      pushList(RL);break;}
    case aStr: {
      RS=Qs2val(valStr(V));L=0;
      for(I=valStrLen(V)-1;I>=0;I--)valStrChr(RS,L++)=valStrChr(V,I);
      push(RS);break;}
    default: gripe("That aint a list, OR a string ");
    }
  dropVal(V);
}

usrFn format(){// single var only, bleh
  val VP;    // the thing to show
  list LV;
  string VFS; // formatStr for printf
  int res;   // did it succeed?
  char Ss[999];
  if(noStk(2)||noStr(1))bail("Need a value and a format-string ");
  VFS=popStr;VP=pop;
  switch(VP.vT){
    case anInt: res=snprintf(Ss,800,strS(VFS),VP.vD.I);break;
    case aReal: res=snprintf(Ss,800,strS(VFS),VP.vD.R);break;
    case aStr:  res=snprintf(Ss,800,strS(VFS),valStr(VP));break;
    default: gripe("format is only for numbers and strings ");
  }
  dropVal(VP);dropStr(VFS);
  if(res>=0)pushQs(Ss);
}//WARNING!  SEGFAULT IF BAD FORMATSTR IS SENT TO SPRINTF
// see https://www.geeksforgeeks.org/printf-in-c/

/*usrFn num2str(){
  val nV;
  char rS[80];
  int i;
  if(noStk(1)||noNum(1))bail("Need a number to make into a string ");
  nV=pop;
  if(isReal(nV)){
    sprintf(rS,"%f",nV.vD.R);
    for(i=strlen(rS)-1;i>=0;i--){
      if(rS[i]=='0')rS[i]=0;else break;
    }
  }
  else sprintf(rS,"%d",nV.vD.I);
  pushQs(rS);
}*/

/*usrFn prf(){ // show a number or string with some formatting
  val VP;    // the thing to show
  string VFS; // formatStr for printf
  if(noStk(2)||noStr(1))bail("Need a value and a formatStr ");
  VFS=popStr;VP=pop;
  switch(VP.vT){
    case anInt: printf(strS(VFS),VP.vD.I);break;
    case aReal: printf(strS(VFS),VP.vD.R);break;
    case aStr:  printf(strS(VFS),valStr(VP));break;
    default: gripe("prf is only for numbers and strings ");
  }
  dropVal(VP);dropStr(VFS);
}*/

usrFn pr(){
  val vp;
  needStk(1,"Need something to show ");vp=pop;
  if(isStr(vp))putStrVal(vp);
//putStr(valStr(vp));//shows it without the quotes
  else showVal(vp);//for a string in a list, show it with quotes
  dropVal(vp);
}

usrFn showStk(){
  int p;
  if(sp)for(p=sp;p>=1;p--){showValMin(stk[p]);putchar(' ');}
  else putStr("(Stack is empty)");
}

usrFn showMem(){
  int i; int a; void* p; list L; obj O; string S; filD* F;
  for(i=1; i<=allocs; i++){
    a = alloc_amts[i]; p = alloc_adrs[i];
    if(alloc_amts[i] != 0){
      printf("@ %p: %d bytes: ",p,a);
      if(p==globs)puts("(Global Variable List)");
      else switch(alloc_types[i]){
        case aList:
          L = p;
          printf("List w/%d item(s), usage=%d\n", listLen(L),listUsage(L));
          break;
        case anObj:
          O = p;
          printf("Obj with %d vars, usage=%d\n", objLen(O),objUsage(O));
          break;
        case aCmd:
          S = p;
          printf("Cmd: ");putStr(&S[si*2]);printf(", usage=%d\n",strUsage(S));
          break;
        case aStr:
          S = p;
          printf("Str: ");shoStrMin(S);printf(", usage=%d\n",strUsage(S));
          break;
        case aFile:
          F = p;
          printf("File: name = "); putStr(F->Name);
          printf(", usage = %d\n", F->Usage);
          break;
        default: puts("dunno what THAT is...");
      }
    }
  }
  printf("Total: %d\n",allocated);
}

usrFn fn2obj(obj OO){
  val V;
  if(noStk(1)||noList(1))bail("Need a list to convert to an object ");
  V=pop;
  sto_new(OO,objMarker,int2val(2));// Drop marker on var list
  ogo(V,OO);                       // Run the subroutine
  pushNewObjFromVars(OO);        // Put any vars it left IN new object
  dropVal(V);
}

usrFn objSplice(){//only used by splice, move code there
  obj voO; obj voN;
  voO=popObj;voN=popObj;
  pushObj(objCat(voN,voO)); // objCat only used once, maybe move it here
  dropObj(voN);dropObj(voO);
}

usrFn strSplice(){ // only used by splice, move it there
  string s1; string s2; string s3; int totLen;
  s2=popStr;s1=popStr;
  totLen=strLen(s1)+strLen(s2);
  s3=newStr(totLen);
  setStrLen(s3,totLen);
  strcpy(strS(s3), strS(s1));
  strcat(strS(s3), strS(s2));
  strChr(s3,totLen)=0;
  pushStr(s3);dropStr(s1);dropStr(s2);
}

usrFn splice(){
  int t1;int t2;
  if(noStk(2))bail("Need 2 things to splice ");
  t1=stk[sp].vT;t2=stk[sp-1].vT;
  if(t1!=t2)bail("2 things to splice must be same type ");
  switch(t1){
    case aStr: strSplice();break;
    case aList: lsplice();break;
    case anObj: objSplice();break;
    default: bail("Can only splice strings, lists, or objects ");
  }
}

usrFn allSplice(){
  list SL; // the list to splice all the stuff from
  int n; val v;
  if(noStk(1)||noList(1))bail("Need a list to splice all the stuff from ");
  SL=popList;
  if(listLen(SL)==0)goto z;
  v=listItem(SL,1);useVal(v);push(v);
  if(listLen(SL)==1)goto z; //no need to splice
  for(n=2;n<=listLen(SL);n++){
    v=listItem(SL,n);useVal(v);push(v);splice();if(BAD)goto z;}
z:dropList(SL);}

usrCmd spliceOntoVar(obj O,char* N){
  val V;
  needStk(1,"Need something to splice onto that variable ");
  V=rclVar(O,N);
  if(!err){push(V);stkSwap();splice();uReplVar(O,N);}
}

usrFn bitshift(int LR){ // whether to shift it left or right
  int n; // The number to bitshift
  int s; // How many bits to shift it
  if(noStk(2)||noInt(1)||noInt(2))
    bail("Need an integer and how many bits to shift it ");
  s=popInt;n=popInt;
  if(LR)pushInt(n<<s);else pushInt(n>>s);
}



//int logicbits=-2147483648; // default (maybe not a good idea after all)

usrFn logic2(int gatetype){
  int x; // The two input ints // these 3 were unsigned long long
  int y; // for the 2-input logic ops
  int r; // the output of whatever op is done
  if(noStk(2)||noInt(1)||noInt(2))bail("Need TWO integers for logic ops ");
  y=popInt;x=popInt;
  switch(gatetype){
    case 0: r=x&y;break;          //bitand
    case 1: r=x|y;break;           //bitor
    case 2: r=~(x&y);break;         //bitnand
    case 3: r=~(x|y);break;          //bitnor
    case 4: r=x^y;break;              //bitxor
    case 5: r=~(x^y);break;            //bitxnor
    case 6: r=x%y;break;                //actually a modulo but whatever...
    case 7: r=x&&y;break;                //and
    case 8: r=x||y;break;                 //or
    case 9: r=!(x&&y);break;               //nand
    case 10: r=!(x||y);break;               //nor
    case 11: r=((x&&(!y))||(y&&(!x)));break; //xor
    case 12: r=((x&&y)||((!y)&&(!x)));break;  //xnor
  }
//  r=r%2147483648; // needs to be a shift-right or something,
 // r=r&logicbits;   // LIMIT how many bits of logic are used
                  // depending on bitwise #-of-bits setting
  pushInt(r);
}//oughta tell it how many bits upfront,or even have global "bits" cFunc

usrFn bitnot(int bitwise){
  unsigned long long o;int b;
  if(noStk(1)||noInt(1))bail("Need an integer to take the logical inverse of ");
  b=popInt;
  if(bitwise)o=~b;else o=!b; //o=~b&logicbits;else o=!b;
  pushInt(o);
}// maybe bitnot should take an int for # of bits to invert?
 // such as 'x 8 nots. No, that can be done with 'x 255 xors

usrCmd logicWithVar(obj O,char* N,int x){
  int I;
  if((x!=5)&&(noStk(1)||noInt(1)))
     bail("Need an integer to do bitwise logic on a variable ");//!x patched-in
  I=findVarInObj(O,N);if(!I)bail("Variable not found:");
  if(notInt(O[I].vV))bail("Bitwise logic requires integer values ");
  push(O[I].vV);
  if(x==5)bitnot(1); else logic2(x);
  if(BAD)return;
  O[I].vV=pop;
}

usrFn int2bin(){ // integer -> string of ones and zeroes
  int n; int L; char s[40]; int minus;
  n=0; memset(s,0,40);
  if(noStk(1)||noInt(1))
    bail("Need an integer to make a string of ones and zeros out of ");
  L=popInt;
  for(n=0;n<=31;n++)if(L&(1<<(31-n)))s[n]='1';else s[n]='0';
  pushQs(s);
}

usrFn bin2int(){ // string of ones and zeroes -> integer
  string bS;   // binary string
  int L;     // its length
  int n;   // for loop count
  int I; // to add up the isNum value
  if(noStk(1)||noStr(1))
    bail("Need a string of ones and zeros to make an integer out of ");
  bS=popStr;L=strLen(bS);//note capital L
  I=0;for(n=0;n<=L;n++)if(strChr(bS,n)=='1')I+=(1<<(L-n-1));//#49="1"
  pushInt(I);dropStr(bS);
}

usrFn cpVal(){ // COPY a val so altering its copy won't alter the original
  val A; list L1; list L2; obj o1; obj o2; int n;
  needStk(1,"Need a value to make a separate copy of ");
  A=stk[sp];  // needn't pop, just gonna put the copy there anyway...
  switch(A.vT){
    case aList:
      if(valListUsage(A)==1)return;//no need if there's only one ref
      L1=A.vD.L;L2=newList(listLen(L1));listLen(L2)=listLen(L1);
      memcpy(&L2[3],&L1[3],listLen(L2)*sizeof(val));//1st 3 are reserved
      stk[sp]=list2val(L2);
      forListNdx(L2,n)useVal(listItem(L2,n));//more usage for the listitems
      dropVal(A);break; // done copying FROM that one...
    case aStr:
      if(strUsage(A.vD.S)==1)return;//no need if there's only one ref
      stk[sp]=Qs2val(valStr(A));dropVal(A);break; //copy str part
    case anObj:
      if(objUsage(A.vD.O)==1)return;//no need if there's only one ref
      o1=A.vD.O;o2=newObj(objLen(o1));objLen(o2)=objLen(o1);
      memcpy(&o2[1],&o1[1],objLen(o1)*sizeof(var));//1st is reserved
      for(n=1;n<=objLen(o2);n++)useVal(o2[n].vV);//each var val must use+1    
      stk[sp]=obj2val(o2);dropVal(A); break;
//  default = nothing since other vals are not allocated
  }// as rebol has, yapi might need a "cp deep" option
}


/*
usrFn str2lines(){ // unneeded; can use [13 10] split
  string s;
  char lb[3];
  char* fc;
  int numsubs;
  list LR;
  lb[0]=13;lb[1]=10;lb[2]=0;
  numsubs=0;
  if(noStk(1)||noStr(1))bail("Need a string to split into lines ");
  stkDup(); // because need it again later
  cpVal();s=popStr;
  fc=strtok(strS(s),lb);
  while(fc!=NULL){
    fc=strtok(NULL,lb);
    if(fc!=NULL)numsubs++;}
  dropStr(s);
  cpVal();s=popStr; //AGAIN, becaues strtok trashed it.
  LR=newList(numsubs);listLen(LR)=numsubs;numsubs=0;
  fc=strtok(strS(s),lb);
  while(fc!=NULL){
    fc=strtok(NULL,lb);
    if(fc!=NULL)listItem(LR,++numsubs)=Qs2val(fc);}
  dropStr(s);pushList(LR);
}
*/

usrFn doInStkObj(){
  obj O2; val sb;
  if(noStk(2)||noObj(2))bail("Need an object and a list of code ");
  sb=pop;
  if(objUsage(stk[sp].vD.O)>1)cpVal(); //Don't mess up the orig.obj
  O2=popObj;
  ogo(sb,O2);
  pushObj(O2);
  dropVal(sb);
}

usrFn trimS(){//ditch leading and trailing spaces
  int p;      // traverse
  int d;      // scratch
  int L;      // length of string
  int stuf;   // does the string actually HAVE anything but spaces?
  string S;
  if(noStk(1)||noStr(1))
    bail("Need a string to trim the leading and trailing spaces off of ");
  if(strUsage(valStr(stk[sp]))>1)cpVal(); // don't alter duplicates
  S=popStr;  L=strlen(S);
  if(L==0){pushStr(S);return;} // no point in trimming an empty string...
  stuf=0;for(p=0;p<L;p++)if(strChr(S,p)!=32)stuf=1;
  if(stuf==0){dropStr(S);pushQs("");return;} // nothing there BUT spaces
  for(p=L-1;p>=0;p--)if(strChr(S,p)!=32)break;else strChr(S,p)=0;
  d=0;for(p=0;p<L;p++){d=p;if(strChr(S,p)!=32)break;}//leaving d as new EOS
  if(d)memcpy(&S[0],&S[d],L-d); // so terminate the string at d
  setStrLen(S,strlen(strS(S)));//mk countedLen = CstrLen
  pushStr(S);
}

usrFn chopS(){//replace cr/lf with spaces
  string S; // the string to chop
  int p;    // traverse
  int L;    // length of str
  int stuf; // does the string HAVE anything other than cr/lf?
  if(noStk(1)||noStr(1))bail("Need a string to remove the cr/lf from ");
  if(strUsage(valStr(stk[sp]))>1)cpVal();
  S=popStr;  L=strLen(S);
  if(L==0){pushStr(S);return;} // blank, just put it back
  stuf=0;for(p=0;p<L;p++)if((strChr(S,p)!=13)&&(strChr(S,p)!=10))stuf=1;
  if(stuf==0){dropStr(S);pushQs("");return;} // nothing there BUT spaces
  for(p=L-1;p>=0;p--)
    if((strChr(S,p)!=13)&&(strChr(S,p)!=10))break;else strChr(S,p)=0;//clip end
  for(p=0;p<L;p++)
    if((strChr(S,p)==13)||(strChr(S,p)==10))strChr(S,p)=32;//conv nl's in middle
  setStrLen(S,strlen(strS(S)));//mk countedLen = CstrLen
  pushStr(S);
}


int ind; // INDIRECT list alteration

usrFn cpListItem(){ // 'L 3 '# -> gets 3rd item from 'L
  list L; // list to copy item out of
  int iN; // item number to copy from
  val cV; // the copied value
  int LAL; // listalloclen holder
  int s=sizeof(val);
  if(noStk(2)||noList(2)||noInt(1))
    bail("Need a list, and the integer index of which item to get ");
  iN=popInt;
//if(ind)cpVal();
  L=popList;
  if(iN==0)iN=-listLen(L)-1; // so it'll still be zero after calc
  if(iN<0)iN=listLen(L)+iN+1; // neg=count from end
  if((iN<1)||(iN>listLen(L))){
    printf("Tried to get item # %d from a %d-item list",iN,listLen(L));
    err=1;goto z;}
//Here also, must make sure there is +1 overcommit, just in case
  if(notList(listItem(L,iN)))goto NL;
  if(listLen(listItemList(L,iN))>=listAllocLen(listItemList(L,iN))){
    if(dbug)puts("Expanding list var while cp");
    listAllocLen(listItemList(L,iN))=(7+listLen(listItemList(L,iN)));//oc
    LAL=listAllocLen(listItemList(L,iN));
    listItemList(L,iN)=(list)realoc(listItemList(L,iN),(3+LAL)*s);
  }
NL:cV=listItem(L,iN);
//-------------made room
  useVal(cV);push(cV);
z:dropList(L);}//ind=0;}

usrFn incDecListItem(int incDec){
  list L; int n;
  if(noStk(2)||noInt(1)||noList(2)){
    gripe("Need a list, and the integer index of an item to ");
    if(incDec==1)putStr("increment");else putStr("decrement");
    return;
  }
  n=popInt;
  if(ind)cpVal();// got a COPY of the list so as not to alter orig 
  L=popList;
  if(notInt(listItem(L,n)))zbail("List needed an integer to inc/dec ");
  listItemInt(L,n)+=incDec;
z:if(ind)pushList(L);else dropList(L);ind=0;}

usrFn doToListItem(int maths){ // 'L 3 "bla" =#
  list L; // list to replace an item in
  int iN; // number of which item to replace
  val rV; // what to replace it with
  if(noStk(3)||noList(3)||noInt(2))
    bail("Need a list, the integer index of an item, and a value ");
  rV=pop; iN=popInt;
  if(ind)cpVal();// got a COPY of the list so as not to alter orig 
  L=popList;
// BUT should only do the cpval if doing INDIRECT repl.list.item
  if(iN==0)iN=-listLen(L)-1; // so it'll still be zero after calc
  if(iN<0)iN=listLen(L)+iN+1; // neg=count from end
  if((iN<1)||(iN>listLen(L))){
    printf("Tried to access item # %d in a %d-item list",iN,listLen(L));
    err=1;goto z;}
  switch(maths){
    case 0: dropVal(listItem(L,iN));
            listItem(L,iN)=rV;break;//(replace)(=#)
    case 1: listItem(L,iN)=add2vals(listItem(L,iN),rV);
            dropVal(rV);break;//(add)(+#)
    case 2: listItem(L,iN)=sub2vals(listItem(L,iN),rV);
            dropVal(rV);break;//(subtract)(-#)
    case 3: listItem(L,iN)=mul2vals(listItem(L,iN),rV);
            dropVal(rV);break;//(mul)(*#)
    case 4: push(listItem(L,iN));push(rV);divd();
            listItem(L,iN)=pop;dropVal(rV);break;//(div)(/#)
    case 5: push(listItem(L,iN));push(rV);logic2(0);
            listItem(L,iN)=pop;dropVal(rV);break;//(and)(&#)
    case 6: push(listItem(L,iN));push(rV);logic2(1);
            listItem(L,iN)=pop;dropVal(rV);break;//(or)(|#)
    case 7: push(listItem(L,iN));push(rV);logic2(4);
            listItem(L,iN)=pop;dropVal(rV);break;//(xor)(^#)
    case 8: push(listItem(L,iN));push(rV);
            listItem(L,iN)=pop;break;//(swap)(%#)
  }
z:if(ind)pushList(L);else dropList(L);ind=0;}

usrFn uCutListItem(){ // fn from <#
  val vL; // has list to cut item from
  int iN; // which item to cut
  val vC; // to put cut item into
  if(noStk(2)||noList(2)||noInt(1))
    bail("Need a list, and the integer index of which item to cut ");
  iN=popInt;
  if(ind)cpVal();//COPY of list
  vL=pop;
  if(iN==0)iN=-valListLen(vL)-1; // so it'll still be zero after calc
  if(iN<0)iN=valListLen(vL)+iN+1; // neg=count from end
  if((iN<1)||(iN>valListLen(vL))){
    printf("Tried to cut item # %d from a %d-item list",iN,valListLen(vL));
    err=1;dropVal(vL);ind=0;return;}
  vC=cutItemFromList(vL,iN);
  if(ind)push(vL);else dropVal(vL);
  push(vC);ind=0;
}//It left the cut item, AND the copy-of-list with that item removed

usrFn delListItem(){
  list L; // list to delete an item out of
  int iN; // number of which item to kill
  int K;  // to span the list
  if(noStk(2)||noList(2)||noInt(1))
    bail("Need a list, and the integer index of which item to delete ");
  iN=popInt;
  if(ind)cpVal();//COPY of list so's not to alter orig
  L=popList;
  if(iN==0)iN=-listLen(L)-1; // so it'll still be zero after calc
  if(iN<0)iN=listLen(L)+iN+1; // neg=count from end
  if((iN<1)||(iN>listLen(L))){
    printf("Tried to delete item # %d from a %d-item list",iN,listLen(L));
    err=1;goto z;}
  dropVal(listItem(L,iN));
  for(K=iN;K<listLen(L);K++)listItem(L,K)=listItem(L,K+1);
  listLen(L)--;
  if(ind){pushList(L);ind=0;return;}
z:dropList(L);ind=0;}

usrFn uInsListItem(){ // IT HAS NO PATH BACK TO THE VARIABLE!
  list L; int iN; val iV;
  if(noStk(3)||noInt(2)||noList(3))
    bail("Need a list, an integer for item#, and a value to insert there ");
  iV=pop; iN=popInt;
  if(ind)cpVal();
  L=popList;

  if((listLen(L)==listAllocLen(L))&&(ind==0)){
    gripe("Must make room with !list instead of 'list when inserting ");
//NOPE!  make each list with +1 overcommit to begin with,
// and check whenever 'list even without using !
// ACTUALLY, now this errormsg shouldn't show
    dropList(L);dropVal(iV);ind=0;err=1;return;}

  if(iN==0){appendList(L,iV);dropVal(iV);goto y;}
  if(iN<0)iN=listLen(L)+iN+1; // neg=count from end
  if((iN<1)||(iN>listLen(L))){
    printf("Tried to insert new item # %d to a %d-item list",iN,listLen(L));
    err=1;dropList(L);dropVal(iV);ind=0;return;}
  L=insN2Lst(L,iV,iN);
y:if(ind)pushList(L);else dropList(L); ind=0;
}

usrFn isit(int COND){int C; C=COND;if(OK)pushInt(C);}//used w/vComp

obj tf(obj vspc){// check condition, select between 2 blox
  int cond; // the condition (true or false)
  val tBlk; // what to do if true
  val fBlk; // what to do if false
  if(noStk(3)||noInt(3)){
    printf("Need a condition(integer)(zero = false, nonzero = true)\n");
    printf("and TWO things to do; one for if it's true\n");
    printf("and the other for if it's false ");
    err=1;return vspc;}
  fBlk=pop;tBlk=pop;cond=popInt;
  if(cond)ogo(tBlk,vspc);else ogo(fBlk,vspc);
  dropVal(tBlk);dropVal(fBlk);return vspc;
}

obj leg(obj o){ // less-than;equal;greater-than[3 blocs]
  val ifLt; val ifEq; val ifGt;
  if(noStk(5)){
    puts("\nNeed 2 values to compare, and 3 things to do,");
    puts("depending on whether the first one is less than,");
    putStr("equal to, or greater than the second one. ");err=1;return o;}
  ifGt=pop;ifEq=pop;ifLt=pop;
  switch(vComp()){//vcomp does its own stkpop of 2 values & compares them
    case  1: ogo(ifLt,o);break;
    case  0: ogo(ifEq,o);break;
    case -1: ogo(ifGt,o);break;
    default: gripe("The 2 values being compared need to be the same type ");
  }
  dropVal(ifLt);dropVal(ifEq);dropVal(ifGt);return o;
}

obj swcase(obj O){// switch/case thing
  list L;          // a list of case-vals and associated case-lists
  val swV;        // the val being compared to all the case-vals
  int N;         // counting
  if(noStk(2)||noList(1)){err=1;
  putStr("Usage: someval caseblock cases ");return O;}
  L=popList;swV=pop;
  if(listLen(L)%2==0)zbail("Bad format for switch/case ");
  for(N=1;N<=listLen(L)-2;N+=2)if(vsame(swV,listItem(L,N))){
    ogo(listItem(L,N+1),O); // case-val matched, so do the thing.
    goto z;} // and there is no "break"; that's automatic
  ogo(listItem(L,listLen(L)),O); // none matched so do the default case
z:dropList(L);dropVal(swV);return O;} // a default case is mandatory

obj t_f(obj vspc,int ft){// if ft=0, do-if-false. if 1, do-if-true
  val stuf; // stuff to do if cond==ft
  int cond;// the val that contains the condition
  if(noStk(2)||noInt(2)){
    printf("Need a condition(integer)(zero = false, nonzero = true)\n");
    printf("and something to do if the condition is ");
    if(ft)printf("true ");else printf("false ");
    err=1;return vspc;};
  stuf=pop;cond=popInt;
  if((ft==0)==(cond==0))ogo(stuf,vspc);
  dropVal(stuf);return vspc;
}

int whileOrUntil; // for use in "loopy" and "doLoopy"

obj doLoopy(obj oO){//it's a while-loop or an until-loop
  int wU;
  val whatToDo;   // the stuff inside the loop
  int loopResult; // loop must return a t/f result each time to see if done
//printf("wu=%d\n",whileOrUntil);
  wU=whileOrUntil;
  if(wU==-1){putStr("doloop needs a while or an until somewhere before it ");
    err=1;return oO;}
  if(noStk(1)||noList(1)){
    putStr("Usage: while/until [some loop body code] doloop ");err=1;return oO;}
  whatToDo=pop;
M:ogo(whatToDo,oO);if(BAD)goto z;
  if(noStk(1)||noInt(1))zbail("[loop body] didn't leave a true or false onstack ");
  loopResult=popInt;
  if(err<0){err++;goto z;}
  if((loopResult==0)==(wU==0))goto M;//loop some MORE
z:dropVal(whatToDo);whileOrUntil=-1;return oO;}//ALWAYS run loop at least once

obj loopy(obj oO){
  int wU;
  val loopBody;
  val condTest;
  int condResult;
  wU=whileOrUntil;//make it local incase sub-loop changes the global one
  if(wU==-1){putStr("loop needs a while or an until somewhere before it ");
    err=1;return oO;}
  if(noStk(2)||noList(1)||noList(2)){
    putStr("Usage: [some loop body code] while/until [condition] loop ");err=1;return oO;}
  condTest=pop;loopBody=pop;
M:ogo(condTest,oO);if(BAD)goto z;
  if(noStk(1)||noInt(1))zbail("Condition must leave true or false ");
  condResult=popInt;
  if(err<0){err++;goto z;}
  if((condResult==0)==(wU==0)){ogo(loopBody,oO);goto M;}
z:dropVal(loopBody);dropVal(condTest);whileOrUntil=-1;return oO;}

char endian; // for use in reading/writing integers in binary format

//"r" Opens a file for reading. The file must exist.
//"w" Creates an empty file for write (erasing any existing file of that name)
//"a" Appends to a file. The file is created if it does not exist.
//"r+" Opens a file to update both reading and writing. The file must exist.
//"w+" Creates an empty file for both reading and writing.
//"a+" Opens a file for reading and appending.
usrFn openAFile(){
  filD* f;   // The File descriptor
  string fnam;  // name of the file to open
  int openMode; // mode identifier
    if(noStk(2)||noStr(2)||noInt(1))
      bail("Need the name of a file to open, and a mode identifier ");
    openMode=popInt;fnam=popStr;
//  if((sp>1)&&hazStr(2)&&hazInt(1)){openMode=popInt;fnam=popStr;}else
//  if((sp>1)&&hazStr(1)&&hazInt(2)){fnam=popStr;openMode=popInt;}else
//    bail("Need the name of a file to open, and a mode identifier ");
  f=(filD*)maloc(sizeof(filD),aFile);
  switch(openMode){
    case 'r': f->Handle=fopen(strS(fnam),"r");break;
    case 'w': f->Handle=fopen(strS(fnam),"w");break;
    case 'a': f->Handle=fopen(strS(fnam),"a");break;
    case 'R': f->Handle=fopen(strS(fnam),"r+");break;
    case 'W': f->Handle=fopen(strS(fnam),"w+");break;
    case 'A': f->Handle=fopen(strS(fnam),"a+");break;
    default:
    putStr("The mode identifier for a file needs to be one of:\n");
    putStr(" \\r (open as read-only)\n");
    putStr(" \\w (create file for write)\n");
    putStr(" \\a (open for append)\n");
    putStr(" \\R (open for read/write)\n");
    putStr(" \\W (create for write/read)\n");
    putStr(" \\A (open for both read/append)\n");
    err=1;fre(f);goto z;}
  if(f->Handle==NULL){
    fre(f);putStr("Couldn't open file ");putStr(strS(fnam));zbail(" ");}
  strcpy(f->Name,strS(fnam));
  f->Mode=openMode;
  f->Usage=1;
  pushFile(f);
z:dropStr(fnam);}

#define fileDesc filD*
#define fModeChk(F,S,E) if(!strchr(S,F->Mode))zbail(E)

usrFn readLineFromTextFile(){
  fileDesc F; char S[400];
  if(noStk(1)||noFile(1))bail("Need a text file to read a line from ");
  F=popFile;
  fModeChk(F,"rRWA","File not opened for read ");
  if(feof(F->Handle))zbail("File is already at its end ");
  if((fgets(S,399,F->Handle))==NULL)S[0]='\0';//hit eof during
  S[strcspn(S,"\n")]='\0';//Clip string at cr/lf
  pushQs(S);
z:dropFile(F);}

usrFn isEof(){
  fileDesc F;
  if(noStk(1)||noFile(1))
    bail("Need an open file to see if it's at its end ");
  F=popFile; pushInt(feof(F->Handle)); dropFile(F);
}

/*usrFn putStrValBinary(){// Needs to have a more standard-looking output
  val s;
  int n;int col;int i;
  unsigned char c;
  if(noStk(1)||noStr(2)||noInt(1))
    bail("Need a string to show as binary, and in how many columns ");
  col=popInt;s=pop;i=0;
  for(n=0;n<valStrLen(s);n++){
    c=(unsigned char)valStrChr(s,n);
    if((c<33)||(c>126))printf("%02X ",c);
    else               printf(" %c ",c);
    if(++i >= col){puts("");i=0;}
  }
  dropVal(s);
}*/

usrFn putStrValBinary(){
  val theStr;  // the stringval to print in hex
  int lenStr;  // how long is the string?
  int numCols; // how many columns to use
  int numRows; // how many rows will there be?
  int strPos;  // where am i in the string?
  int rowNum;  // which row?
  int colNum;  // which column?
  unsigned char c;  // just a char from in the string
  if(noStk(1)||noStr(2)||noInt(1))
    bail("Need a string to show as binary, and in how many columns ");
  numCols=popInt;
  theStr=pop;
  lenStr=valStrLen(theStr);
  numRows=ceil(lenStr/numCols);
  putStr("      ");
  for(colNum=0;colNum<numCols;colNum++)printf("%2d ",colNum);
  puts("");
  for(rowNum=0;rowNum<=numRows;rowNum++)if(rowNum*numCols<lenStr){
    printf("%4d: ",rowNum*numCols);
    for(colNum=0;colNum<numCols;colNum++){
      if(rowNum*numCols+colNum<lenStr){
        c=(unsigned char)valStrChr(theStr,rowNum*numCols+colNum);
        printf("%02X ",c);
      }else putStr("   ");
    }
    for(colNum=0;colNum<numCols;colNum++){
      if(rowNum*numCols+colNum<lenStr){
        c=(unsigned char)valStrChr(theStr,rowNum*numCols+colNum);
        if((c<33)||(c>126))putchar('.');else putchar(c);
      };
    }
    puts("");
  }
  dropVal(theStr);
}

usrFn getUserInput(){
  char s[255];
  fgets(s,254,stdin);
  s[strcspn(s,"\n")]='\0'; // clip str at enter
  pushQs(s); // it might later be push detect(s);
}

usrFn charsFromFile(){
  fileDesc f; int n; int howMany; string stuff;
  if(noStk(2)||noInt(1)||noFile(2))
    bail("Need an open file, and how many chars to read from it ");
  howMany=popInt;
  if(howMany<0)bail("Can't read a NEGATIVE-length string ");
  f=popFile;
  fModeChk(f,"rRWA","File not opened for read ");
  stuff=newStr(howMany);
  n=fread(strS(stuff),1,howMany,f->Handle);  // read the data
  setStrLen(stuff,n);
  strChr(stuff,n)=0;
  pushStr(stuff);
z:dropFile(f);}//these need to also work on strings as well as files

// need also fscanf and fprintf going

usrFn frdInt(){ // len from stk, deal with sign/unsign afterwards
  fileDesc f; int iLen; long int X; int n;
  if(noStk(2)||noInt(1)||noFile(2))
    bail("Need an open file, and what size integer to read ");
  iLen=popInt;
  if((iLen<1)||(iLen>4))bail("1byte thru 4byte integers only ");
  f=popFile;X=0;
  fModeChk(f,"rRWA","File not opened for read ");//uses label z
  fread(&X,1,iLen,f->Handle);
  if(endian)switch(iLen){
    case 2:
      X=((X&0xFF00)>>8)+((X&0xFF)<<8);break;
    case 3:
      X=((X&0xFF0000)>>12)+(X&0xFF00)+((X&0xFF)<<12);break;
    case 4:
      X=((X&0xFF000000)>>16)+((X&0xFF0000)>>8)+((X&0xFF00)<<8)+((X&0xFF)<<16);
    break;
  }
z:dropFile(f);pushInt(X);}

usrFn fwrInt(){ // len from stk, deal with sign/unsign afterwards
  fileDesc f; int iLen; long int X; int n;
  if(noStk(3)||noInt(1)||noFile(2)||noInt(3))
    bail("Need an integer, an open file, and how many bytes to write ");
  iLen=popInt;
  if((iLen<1)||(iLen>4))bail("1byte thru 4byte integers only ");
  f=popFile;X=popInt;
  fModeChk(f,"wRWA","File not opened for write ");//uses label z
  if(endian)switch(iLen){
    case 2:
      X=((X&0xFF00)>>8)+((X&0xFF)<<8);break;
    case 3:
      X=((X&0xFF0000)>>12)+(X&0xFF00)+((X&0xFF)<<12);break;
    case 4:
      X=((X&0xFF000000)>>16)+((X&0xFF0000)>>8)+((X&0xFF00)<<8)+((X&0xFF)<<16);
    break;
  }
  fwrite(&X,1,iLen,f->Handle);
z:dropFile(f);}//might need to change the order of args on this one

usrFn fileSeek(int absSeek){//note: YAPI file seeks start at 1, not 0
  fileDesc F; long int I;
  if(noStk(2)||noInt(1)||noFile(2)){
    if(absSeek){bail("Need an open file, and what position in it to seek ");}
    else{bail("Need an open file, and how far (+-) in it to skip ");}}
  I=popInt;F=popFile;
  if(absSeek){
    if(I<0)fseek(F->Handle,0,SEEK_END);//append???
//  Since seekbase 0 is standard, CAN'T do the "seek from end" thing
    else   fseek(F->Handle,I,SEEK_SET);
  }else fseek(F->Handle,I,SEEK_CUR);//since NOONE uses seekbase 1
  dropFile(F);
}

usrFn filePos(){
  fileDesc F;
  if(noStk(1)||noFile(1))bail("Need an open file to tell the position of ");
  F=popFile;pushInt(ftell(F->Handle));dropFile(F);//+1?nope
}

usrFn bload(){
  if(noStk(1)||noStr(1))bail("Need filename ");
  pushInt('r');openAFile();
  if(BAD)return;
  stkDup();len2int();
//BUG!  if youtry to bload a folder, it allocates NEGATIVE 2 BILLION?
//so...
  if((stk[sp].vD.I>1000000000)||(stk[sp].vD.I<0))
    bail("Not a file. Might be a folder. Can't bload that. ");
  charsFromFile();
}

usrFn charsToFile(){
  fileDesc f; int howMany; string stuff;
  if(noStk(2)||noStr(2)||noFile(1))
    bail("Need a string and an open file to write it to ");
  f=popFile;stuff=popStr;howMany=strLen(stuff);
  fModeChk(f,"wRWA","File not opened for write ");
  fwrite(strS(stuff),1,howMany,f->Handle);
z:dropFile(f);dropStr(stuff);}

usrFn bsave(){
  if(noStk(2)||noStr(1)||noStr(2))
    bail("Need a string to save and a filename to save it to ");
  pushInt('w');openAFile();if(BAD)return;charsToFile();
}

usrFn osCmd(){
  string s;
  int sysResult;
  if(noStk(1)||noStr(1))bail("Need a command string to send to the system ");
  s=popStr;
  sysResult=system(strS(s));
  pushInt(sysResult>>8);
  dropStr(s);
}

usrFn list2str(){
  list L;  // the input list
  int LL;   // its listLength
  string S;  // output string
  int c;      // each int in the list to become a char in the string
  int I;       // span the list
  int allByte=1;// were all the values bytes from 0-255 ?
  if(noStk(1)||noList(1))bail("Need a list to convert into a string ");
  L=popList; LL=listLen(L); S=newStr(LL);
  for(I=1;I<=LL;I++){
    if(isInt(listItem(L,I)))c=listItemInt(L,I);else{c=88;allByte=0;}//'X's
    if((OK)&&((c<0)||(c>255)))allByte=0;//set flag now, complain later
    strChr(S,I-1)=c%256;} // if non-byte ints, force them to be bytes
  strChr(S,LL)=0;// NULL-terminate the string
  setStrLen(S,LL);
  pushStr(S); // yep, even if it's gibberish
  dropList(L);
  if(!allByte)gripe("Some of the list items were not character-compatible ");
}

usrFn split(){
  string s;
  string delim;
  int i;
  char* fc;
  int numsubs;
  list LR;
  numsubs=0;
  if(noStk(2)||noStr(2))bail("Need a string to split, and a delimiter ");
  if(isInt(stk[sp])){
    i=popInt;delim=newStr(1);
    strChr(delim,0)=(char)i;strChr(delim,1)=0;}
  else if(isList(stk[sp])){list2str();if(BAD)return;delim=popStr;}
  else if(isStr(stk[sp]))delim=popStr;
  else bail("Delimiter needs to be a string, integer, or list of integers ");
  stkDup(); // because need it again later
  cpVal();s=popStr;
  fc=strtok(strS(s),strS(delim));if(fc)numsubs++;
  while(fc){
    fc=strtok(NULL,strS(delim));
    if(fc)numsubs++;}
  dropStr(s);cpVal();s=popStr; //AGAIN, becaues strtok trashed it.
  LR=newList(numsubs);listLen(LR)=numsubs;numsubs=0;
  fc=strtok(strS(s),strS(delim));if(fc)listItem(LR,++numsubs)=Qs2val(fc);
  while(fc){
    fc=strtok(NULL,strS(delim));
    if(fc)listItem(LR,++numsubs)=Qs2val(fc);}
  dropStr(s);pushList(LR);dropStr(delim);
}

usrFn str2list(){
  string s; // the input string
  list L;  // raw list to house chars in the string
  int W;  // size of the list to make
  int n; // span the string
  if(noStk(1)||noStr(1))bail("Need a string to make into a list ");
  s=popStr;W=strLen(s);L=newList(W);
  for(n=0;n<W;n++)appendList(L,int2val((unsigned char)strChr(s,n)));
  pushList(L);dropStr(s);
}

usrFn range(){
  int A;  // range start #
  int B;  // range end #
  list L; // raw list to hold the range of integers
  int n;  // counting across the range
  int i;  // counting along the list to be made
  if(noStk(2)||noInt(1)||noInt(2))bail("Need 2 integers to define bounds ");
  B=popInt;A=popInt;
//typically, range end would be more than range start
  if(A<B){
    L=newList(B-A+1);listLen(L)=B-A+1;i=0;//setup empty list
    for(n=A;n<=B;n++){listItemType(L,++i)=anInt;listItemInt(L,i)=n;}
    pushList(L);return;}
//but if start is more, don't swap them, do the whole list downwards
  if(A>B){
    L=newList(A-B+1);listLen(L)=A-B+1;i=A-B+2;
    for(n=B;n<=A;n++){listItemType(L,--i)=anInt;listItemInt(L,i)=n;}
    pushList(L);return;}
//and if they're EQUAL, then it's just a list of one item
  L=newList(1);listLen(L)=1;listItemType(L,1)=anInt;
  listItemInt(L,1)=A;pushList(L);
}

usrFn dim(){
  int aSize; val initV; list L; int n;
  if(noStk(2)||noInt(1))bail("Need init value and list length ");
  aSize=popInt;initV=pop;
  L=newList(aSize);
  for(n=1;n<=aSize;n++)appendList(L,initV);
  pushList(L);dropVal(initV);
}

usrFn each(obj OO){ // [list] [subrt] bitesize ,each(seems to take nested ok)
  val Sbrt;    // subroutine to run on each item in a list
  list Lst;    // Input list to feed to it
  val Li;      // each item in that list
  int N;       // how many items to feed in at a time
  int y;       // stepping thru the list N at a time
  int b;       // counting to N in each step
  if(noStk(3)||noInt(1)||noList(2)||noList(3))
    bail("Need a list, [what to do each loop], and how many items per loop ");
  N=popInt;Sbrt=pop;Lst=popList;
  y=1;while(y<=listLen(Lst)){
    for(b=N-1;b>=0;b--){
      Li=listItem(Lst,y+b); // get each item
      useVal(Li);  // make sure its usage gets incremented
      push(Li);}   // put it onstack for subroutine to use
    y+=N;          // inc it over by the bite size
    ogo(Sbrt,OO);   // run the subrt, which is supposed to use the item
    if(err)goto z;}// break out of the loop if there's a problem
z:dropList(Lst);dropVal(Sbrt);}//??BUG? should this return obj?

usrFn reduce(obj OO){
  val V;           // not "list" because "go" takes a val not a list
  if(noStk(1)||noList(1))bail("Need list to reduce "); V=pop;
  pushMkr(aList);  // drop marker on the stack
  ogo(V,OO);        // run the list as a subroutine
  pushListFromStk; // and collect anything it left ontstack into a list
  dropVal(V);//should this return obj?
}

obj loops(obj OO){
  val toDo; int LC; int howMany;
  if(noStk(2))zbail("Need something to do, and how many times ");
  if(noInt(1))zbail("Number of loops needs to be an integer ");
  howMany=popInt; toDo=pop;
//might check if V.vT is object and run @object ,default
  for(LC=1;LC<=howMany;LC++){ogo(toDo,OO);
  if(err!=0){err++;break;}}//might leak here?
  dropVal(toDo);
z:return OO;}

obj floop(obj oO){// FOR loop with two syntaxes
  int count; int cmin; int cmax; int sTs; val LB; string vN;
  if(noStk(5)||noStr(4))goto B;
  if(noList(5)||noInt(3)||noInt(2)||noInt(1))goto P;
  if(stk[sp].vD.I==0)
    zbail("A for-loop with a step-size of zero would never exit ");
  sTs=popInt;cmax=popInt;cmin=popInt;vN=popStr;LB=pop;
  if(sTs<0)sTs=-sTs;//quietly fix wrong-way stepsize
  sto_new(oO,strS(vN),zilch);
  if(cmax>cmin)for(count=cmin;count<=cmax;count+=sTs){
    oO[findVarInObj(oO,strS(vN))].vV.vD.I=count;ogo(LB,oO);
    if(err!=0){err++;goto x;}}
  else for(count=cmin;count>=cmax;count-=sTs){
    oO[findVarInObj(oO,strS(vN))].vV.vD.I=count;ogo(LB,oO);
    if(err!=0){err++;goto x;}}
x:dropVal(LB);dropVal(cutVar(oO,strS(vN)));dropStr(vN);
z:return oO;
B:if(noStk(4)||noList(4)||noInt(3)||noInt(2)||noInt(1))goto P;
  if(stk[sp].vD.I==0)
    zbail("A for-loop with a step-size of zero would never exit ");
  sTs=popInt;cmax=popInt;cmin=popInt;LB=pop;
  if(sTs<0)sTs=-sTs;//quietly fix wrong-way stepsize
  if(cmax>cmin)for(count=cmin;count<=cmax;count+=sTs)
    {pushInt(count);ogo(LB,oO);if(err!=0){err++;goto y;}}
  else for(count=cmin;count>=cmax;count-=sTs)
    {pushInt(count);ogo(LB,oO);if(err!=0){err++;goto y;}}
y:dropVal(LB);return oO;
P:puts("Usage: [loopbody] :varname startInt endInt stepInt floop ");
  puts(".. or, alternatively ..");
  putStr("Usage: [loopbody] startInt endInt stepInt floop ");
  zbail("");
}

usrFn stkArgs(){
  int needed; string errmsg;
  if(noStk(1)||noInt(1)||noStr(2))
    bail("Need to know minimum # of items onstack, and errmsg if not ");
  needed=popInt; errmsg=popStr;
  if(noStk(needed)){
    if(needed>1){
      printf("%d inputs needed",needed);
      if(sp>0)printf(" but only got %d",sp);
      puts("");}
    else puts("An input was needed");
    gripe(strS(errmsg));puts("");}
  dropStr(errmsg);
}

usrFn getIO(char io){//io=1->getIO. io=0->freeiO.
  int p;   // first port #
  int n;   // how many ports to reserve
  int pok; // span the "portsok" list
  if(noStk(2)||noInt(1)||noInt(2))
    bail("Need to know what port and how many ");
  n=popInt; p=popInt;
  if(ioperm(p,n,io))
    bail("Couldn't reserve the ports (already taken or need root access) ");
  for(pok=p;pok<=(p+n-1);pok++)portsok[pok]=io;//update the list
}

usrFn byteout(){ // output a byte on a port
  int p;  // the port#
  int b;  // the byte
  if(noStk(2)||noInt(1)||noInt(2))
    bail("Need a byte, and a hardware port number to send it to ");
  p=popInt;b=popInt;
  if(portsok[p]==0)bail("You didn't reserve that port with getio ");
  outb(p,b);//actually output the byte now that the safety chex are done
}

usrFn bytein(){ // GET a byte from a port
  int p; // the port number
  if(noStk(1)||noInt(1))
    bail("Need a hardware port #(integer) to read from ");
  p=popInt;
  if(portsok[p]==0)bail("You didn't reserve that port with getio ");
  pushInt(inb(p));//"inb" is absolute-hardware-port input
}

usrFn showVars(obj vars){
  int x; int p; val v;
  if(objLen(vars))for(x=1;x<=objLen(vars);x++){
    if(dbug) printf("@%p %s: ",&vars[x],vars[x].vN);
    else     printf(" %s: ",vars[x].vN);
    v=vars[x].vV;
    showValMin(v);
    printf("\n");
  } else puts("(no vars)");
}

usrFn map(obj vars){ // (list)(subrt)map -> 1:1 item mapping (nest ok)
  list La; // input list
  list Lb; // output list
  val Sb;  // subroutine that maps input list to output list
  val V;   // each one
  val V2;  // going into the resulting list(bugfix may 2024)
  int y;   // counting
  if(noStk(2)||noList(1)||noList(2))
    zbail("Need [some code] for each item, and list of input values ");
  Sb=pop;
  La=popList;
  Lb=newList(listLen(La));
  forListNdx(La,y){ // proc each item
    V=listItem(La,y);useVal(V);push(V);
    ogo(Sb,vars);
    if(BAD){dropList(La);dropVal(Sb);dropList(Lb);return;}
    V2=pop;appendList(Lb,V2);dropVal(V2);} //and its result goes in new list
  pushList(Lb);dropList(La);dropVal(Sb);
z:return;
}//need provision for if data is a string instead of a list,
//and also for possibly feeding more than one item at a time


usrFn tellType(){
  val V; needStk(1,"Need a value to determine the type of "); V=pop;
  tsay(V.vT); dropVal(V);
}

usrFn requireType(int nT){int gT; //see also: stkargs, chkstk
  STRICT=nT; // set a tmp flag
  LENIENT=0; // not lenient yet
  if(noStk(1)){
    putStr("\nGot nothing at all where ");
    tsay(nT);
    putStr(" was needed.");err=1;return;}
  gT=stk[sp].vT;
  if(gT==nT)return; // with no error since the types matched up
  if(nT==aNum){
    if((gT==anInt)||(gT==aReal))return; // with no error since both are NUM
    gripe("Got ");tsay(gT);putStr(" where a number was needed ");
    return;}
  gripe("Got ");tsay(gT);putStr(" where ");tsay(nT);putStr(" was needed ");
}

void Here(){
  char cwd[800];
  int n;
  getcwd(cwd, sizeof(cwd));
  n=strlen(cwd);cwd[n]=cwd[0];cwd[n+1]=0; // tack / on end
  pushQs(cwd);
}

void mez(char* s){  // accept a C string and try to run it
  val fn;
  pushMkr(aList);
  tokenize(s);
  fn=list2val(mkListFromStk());
  ogo(fn,globs); // or maybe (fn,O) ???
  dropVal(fn);
}

usrFn evalStr(){ // get string from stack and try to run it
  string S;
  val fn;
  if(noStk(1)||noStr(1))bail("Needed a string to evaluate ");
  S=popStr;
  pushMkr(aList);
  tokenize(strS(S));
  fn=list2val(mkListFromStk());
  ogo(fn,globs); // or maybe (fn,O) ???
  dropVal(fn);
  dropStr(S);
} // maybe shoulda just fed it to "mez(strS(S))" ??

usrFn breakout(){
  int i;
  if(noStk(1)||noInt(1))goto z;
  err=1-popInt;
  if(err<=0)return;
z:gripe("Needed a positive number for how many brackets to break out of ");
}

usrFn hep(){
  printf("Built-in functions:  ");
  int n;for(n=0;n<=numFuncs;n++)printf("%s ",cFuncs[n]);
  printf("\nVar-use cmds(example var 'x'):  ");
  putStr("'x `x ;x =x <x >x !x +x -x *x /x ^x ");
  puts("$x %x ?x ,x @x &x |x .x ++x --x ~x");
  puts("(Type help to read yapi.txt)");
}

obj uRclVar(obj vars, char* varName){//must use custom rcl incase >#
  val V;
  int F;   // Where in the list of vars was the varName found?
  int li;  // an index for a list
  int vLAL;
//if varname is a number, it's actually a stack-item
  if(F=findVarInStk(varName)){V=stk[F];useVal(V);goto y;}
//otherwise it's just a regular varaiable
  else if((F=findVarInObj(vars,varName))==0)zbail("Not found:");
//HERE is where must make sure there is room to insert 1 item to a LIST
  if(isList(vars[F].vV)&&(valListLen(vars[F].vV)>=valListAllocLen(vars[F].vV)))
  {
    if(dbug)puts("Expanding list var just in case it gets a new item");
    valListAllocLen(vars[F].vV)=(7+valListLen(vars[F].vV)); //mem O.C.
    vLAL=valListAllocLen(vars[F].vV);
    vars[F].vV.vD.L=(list)realoc(vars[F].vV.vD.L,(3+vLAL)*sizeof(val));//3 for hdr
  }
  useVal(vars[F].vV);V=vars[F].vV;
//-------------made room
y:if(OK)push(V);
z:if(err)putStr(" variable to get: ");
return vars;}

usrCmd uSwpVar(obj vars, char* varName){
  val V;
  needStk(1,"Need a value to swap ");
  V=rclVar(vars,varName);
  if(!err){uReplVar(vars,varName);push(V);}else putStr(" variable ");
}

usrCmd uCutVar(obj vars, char* varName){
  val V;V=cutVar(vars,varName);
  if(OK)push(V);else putStr(" variable to cut: ");
}

usrCmd uDelVar(obj vars, char *varName){
  val V;V=cutVar(vars,varName);
  if(OK)dropVal(V);else putStr(" variable to delete: ");}

obj uRunVar(obj vars, char* varName){
  val V; V=rclVar(vars,varName);
  if(OK)ogo(V,vars);else putStr(" user-function to run: ");
  dropVal(V);return vars;
}//a uRunVar-specific brk could be the same as "return" ??

usrFn askType(int T){
  val v; int R;
  if(noStk(1)){
    if(T==anInt)bail("Is WHAT an integer?  (There's nothing there) ");
    if(T==aReal)bail("Is WHAT an real number?  (There's nothing there) ");
    if(T==aNum)bail("Is WHAT a number?  (There's nothing there) ");
    if(T==aStr)bail("Is WHAT a string?  (There's nothing there) ");
    if(T==anObj)bail("Is WHAT an object?  (There's nothing there) ");
    if(T==aList)bail("Is WHAT a list?  (There's nothing there) ");
    if(T==aFile)bail("Is WHAT a file?  (There's nothing there) ");
    bail("Error: Something caused the 'askType' function to crash ");
  }
  v=pop;
  if(T==aNum)R=(isNum(v));else R=(v.vT==T);
  dropVal(v);pushInt(R);
}

usrFn notZero(){
  needStk(1,"Is WHAT not zero? ");
  pushInt(1-zero(pop));
}

usrFn isItZero(){
  needStk(1,"Is WHAT zero? ");
  pushInt(zero(pop));
}

usrFn listItems(){//Given a list, dumps out all the items onStk
  list L; uint n; val v;
  if(noStk(1)||noList(1))bail("Need a list to get the items from ");
  L=popList;
  for(n=listLen(L);n>=1;n--){v=listItem(L,n);useVal(v);push(v);}
  dropList(L);
}

usrFn listSelect(){
  list lFrom; list lNdxs; list lResult; uint n;
  if(noStk(2)||noList(1)||noList(2))
    bail("Need a list of items and a list of which # items to select ");
  lNdxs=popList;lFrom=popList;lResult=newList(listLen(lNdxs));
  forListNdx(lNdxs,n){
    if(notInt(listItem(lNdxs,n)))
      zbail("List of indexes has to be all integers ");
    appendList(lResult,listItem(lFrom,listItemInt(lNdxs,n)));
  }//collect certain selected items from a list into another list
z:dropList(lFrom);dropList(lNdxs);pushList(lResult);}

usrFn CD(){
  string destDir;
  if(noStk(1)||noStr(1))bail("CD to where? ");
  destDir=popStr;
  if(chdir(strS(destDir))<0)gripe("Couldn't CD there ");
  dropStr(destDir);
}

obj doBlk(obj vars){
  val toDo;
  if(noStk(1)||noList(1))zbail("Need a list of code to do ");
  toDo=pop;
  ogo(toDo,vars);
  dropVal(toDo);
z:return vars;}

/*usrFn get1st(){//only used for referencing commands w/o running them
  list L; // yep, the command must be encapsulated inside a list
  if(noStk(1)||noList(1))bail("Usage: [somecommand]: (Deprecated. use ~somecommand)");
  L=popList;
  if(listLen(L)<1)zbail("There's nothing in there ");
  push(listItem(L,1));//first(and assumedly only) item
  useVal(stk[sp]);
z:dropList(L);}*/

usrFn doFileOrDir(char* CB){
  if(!strcmp(CB,"."))--CB;//strcpy(CB,"..")might cause segfault
  if(chdir(CB)<0){pushQs(CB);pushQs(".pf");splice();doFile(1);}
}//this needs to parse file/path stuff better

//usrFn chkStk(){ see earlier version; routined was dropped

obj makeOrReplaceVar(obj oo, char* s){
  int yn;
  doesItExist(oo,s);yn=popInt;
  if(yn==0)return(uMkVar(oo,s));
  uReplVar(oo,s);return oo;
  // ???? uReplVar might need to use varspace too
}

/*
  usrFn startServer(){ //network
  int portNo;
  if(noStk(1)||noInt(1))bail("Needed an integer for port number ");
  portNo=popInt;
//printf("portno=%d\n",portNo);   // Optional diagnostic
  if(listeningTo==portNo)return;  // Don't close-and-reopen same port.
  if(listeningTo){close(servSock);listeningTo=0;} // Only 1 port at a time.
  if(portNo==0)return;            // 0 listen = don't.
  adrPort.sin_port=htons(portNo); // "htons"=get endian right for TCP
  servSock=socket(AF_INET,SOCK_STREAM,0);  // def socket (SOCK_STREAM=1)
  if(bind(servSock, &adrPort, sizeof(adrPort)) == -1){ // if bad
    close(servSock);
    printf("Access denied to port #%d",portNo);
    bail("Something else is still using it, or a timeout is imposed ");}
  listen(servSock, maxBacklog); // If it DID work, set socket to listen
  listeningTo=portNo;
}

usrFn getQuery(){
  if(heard)bail("Haven't replied to previous thing yet ");
  if(listeningTo==0)bail("Need to be listening to a port for that to work ");
  surfer=accept(servSock,0,0);  // Wait for incoming connection.
  memset(inBuf,0,inBufSize);    // Init input buffer to nulls first.
  drip=recv(surfer,inBuf,inBufSize-1,0);// Get msg. drip=how much it got.
  if(drip<0)pushQs("error");
  else if(drip==0)pushQs("blank");
  else pushQs(inBuf);
  heard=1;
}//this needs to be separated into "accept" and "hear" ?? (so not only http)

void reply(char* s){
  if(heard==0)bail("Need to have heard something to reply TO ");
  if(listeningTo==0)bail("Need to be listening to a port for that to work ");
//if(drip==-1)return; // bypass error?
  if(strlen(s)==0)drip=0; // nothing there so don't bother
  else drip=(send(surfer,s,strlen(s),MSG_NOSIGNAL));
};

void startreply(){
  reply("HTTP/1.0 200 OK\r\nContent-Type: ");
  reply(mimeStr);
  reply("\r\n\r\n");
}

#define endreply close(surfer)

usrFn replyHTML(){
  string S;
  if(listeningTo==0)bail("Need to be listening to a port for that to work ");
  if(heard==0)bail("Need to have heard something to reply TO ");
  if(noStk(1)||noStr(1))bail("Need a string for HTML reply ");
  S=popStr;
  startreply();
  if(drip<0){puts("(Error sending header)");goto z;}
//reply(strS(S));//diagnostic
  drip=(send(surfer,strS(S),strLen(S),MSG_NOSIGNAL));
  if(drip<0){puts("(Error sending reply)");goto z;}
z:endreply;heard=0;dropStr(S);}

*/
static const char uT[256] = {
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,
   0, 1, 2, 3, 4, 5, 6, 7,  8, 9,-1,-1,-1,-1,-1,-1,
  -1,10,11,12,13,14,15,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,10,11,12,13,14,15,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1
};//hex codes

usrFn urlDec(){
  string eStr; string dStr;
  char c, a, b; char *i, *o;
  if(noStk(1)||noStr(1))bail("Needed a URL string to decode ");
  eStr=popStr;i=strS(eStr);
  dStr=newStr(strLen(eStr));o=strS(dStr);
  if(i)while(c=*i++)
    if(c=='%')
      if(((a=uT[(unsigned char)*i++])<0)||((b=uT[(unsigned char)*i++])<0))
        {*o++ = c; *o++ = a; *o++ = b;} // not a hex code, just pass it thru
      else
        {c = (a<<4)|b; *o++ = c;} // that was hex code, so convert it
    else *o++ = c;
  *o = '\0';
  setStrLen(dStr,o-strS(dStr));
  dropStr(eStr);pushStr(dStr);
};
/*
usrFn setMimeStr(){
  string M;
  if(noStk(1)||noStr(1))bail("Need string for mime type ");
  M=popStr;
  strcpy(mimeStr,strS(M));
  dropStr(M);
}
*/

usrFn yapiInfo(){
  puts("Info about YAPI internal variables and stuff:");
  printf("# of intrinsic functions: %d\n",numFuncs);
  puts("Includes:stdlib.h,stdio.h,math.h,string.h,unistd.h,time.h");
//n   puts("and for networking, sys/socket.h and arpa/inet.h");
//n   printf("Net inbuf size: %d\n",inBufSize);
  printf("Max allocation units via #define allocs: %d\n",allocs);
  printf("Max Stack via #define maxStk: %d\n",maxStk);
  printf("Max global vars via #define maxGlob: %d\n",maxGlob);
  printf("Single value size: %ld\n",sizeof(val));
  printf("Single variable size: %ld\n",sizeof(var));
}



// sorting
int sortcomp(const void* a, const void* b) {
  useVal(*(val*)b);push(*(val*)b);
  useVal(*(val*)a);push(*(val*)a);
  return vComp();
}
usrFn uSort(){
  list list2Sort;
  if(noStk(1)||noList(1))bail("Need a list to sort ");
  list2Sort=popList;
  qsort(&listItem(list2Sort,1),listLen(list2Sort),sizeof(val),sortcomp);
  pushList(list2Sort);
}
// end of sorting



//---------------------------------<GO: the main loop>
void pushToken(char* w);

obj go(val subrt, obj o){
  val V;    // scratch
  int IP;   // index of which token inside the list
  list Lp;  // the list itself that the var points to
  char* CB; // cmdBody
  if(notList(subrt)){useVal(subrt);push(subrt);return o;};
  Lp = subrt.vD.L;
  forListNdx(Lp,IP)switch(listItemType(Lp,IP)){
    case aCmd:
      CB = &listItemStr(Lp,IP)[si*2+1]; // the beheaded string
      switch(listItemStr(Lp,IP)[si*2]){ // that first char
        case '?': doesItExist(o,CB);break;
        case '>': o=uMkVar(o,CB);break;//maybe take subrt into account?
        case '`': o=makeOrReplaceVar(o,CB);break;//so's not to duplicate
        case ';': uDelVar(o,CB);break;
        case  39: uRclVar(o,CB);break;//39='(singlequote)
        case '<': uCutVar(o,CB);break;
        case ',': o=uRunVar(o,CB);break;
        case '=': uReplVar(o,CB);break;
        case '.': doFileOrDir(CB);break;
        case '@': o=doInObj(o,CB);break;
        case '%': uSwpVar(o,CB);break;
        case '$': spliceOntoVar(o,CB);break;
        case '+': addToVar(o,CB);break;   // unless ++
        case '-': subFromVar(o,CB);break; // unless --
        case '*': mulByVar(o,CB);break;
        case '/': divByVar(o,CB);break;
        case '&': logicWithVar(o,CB,0);break; //bitand-with-var
        case '|': logicWithVar(o,CB,1);break; //bitor
        case '^': logicWithVar(o,CB,4);break; //bitxor
        case '!': logicWithVar(o,CB,5);break; //bitnot?
        case ':': pushQs(CB);break; // just a shortcut one-word string
        case '~': pushToken(CB);if(BAD)pop;break;
//      case '{': IDK();break;
//      case '}': doThingToList();break;
//      case '_': whatever();break;
//  guess'n that's all the punctuations?
//  if any got recognized as a cmd, yet somehow have a different 1st char:
        default: printf("unrecognized command: ");puts(CB);}
      if(BAD){
        printf("\"%s\"\nWord %d in ",&listItemStr(Lp,IP)[si*2],IP);
        return o;}
      break;
    case anInt:
    case aReal: push(listItem(Lp,IP));break;//no need to "use" a number
    case aStr:
    case aList: useVal(listItem(Lp,IP));push(listItem(Lp,IP));break;
    case aCfunc: switch(listItemInt(Lp,IP)){
      case 0: format();break; // sprintf?
      case 1: pr(); break;
      case 2: showStk(); break;
      case 3: o=loops(o);break;
      case 4: stk_inc();break;
      case 5: showMem();break;
      case 6: cpVal();break;
      case 7: while(sp>0)dropVal(pop);break; //clr
      case 8: showVars(o);break;
      case 9: stkDup();break;
      case 10: stkSwap();break;
      case 11: dbug = 1; break;
      case 12: dbug = 0; break;
      case 13: doFile(1);break; // might need to be o=doFile(o)
      case 14: puts("");break;       // nl
      case 15: putchar(' ');break;   // sp
      case 16: uInsListItem();break; // >#
      case 17: uCutListItem();break; // <#
      case 18: cpListItem();break;   // '#
      case 19: delListItem();break;  // ;#
      case 20: doToListItem(0);break; // =#
      case 21: udrop();break;   // drop
      case 22: clrVars();break;       // new
      case 23: isit(vComp()==0);break; // eq
      case 24: o=tf(o);break; // might need o=tf(o)
      case 25: o=doLoopy(o);break; // do-loop
      case 26: isit(vComp()==1);break;  // lt
      case 27: isit(vComp()==-1);break; // gt
      case 28: isit(vComp()<1);break;   // lte
      case 29: isit(vComp()>-1);break;  // gte
      case 30: isit(vComp()!=0);break;  // neq
      case 31: openAFile();break;          // open a file
      case 32: charsFromFile();break;
      case 33: range();break;
      case 34: reduce(o);break; // o=reduce(o) ?
      case 35: map(o);break; // might need o=vo here
      case 36: mul2();break;
      case 37: xmul();break;
      case 38: dmul();break;
      case 39: subt();break; // subtract
      case 40: divd();break; // divide
      case 41: negv();break; // negate
      case 42: invn();break; // x = 1/x
      case 43: fn2obj(o);break;
      case 44: freeallports();
//n            if(listeningTo)close(servSock); //networking
               exit(0);break;//quit
      case 45: o=t_f(o,1);break; // t // might need o=vo here
      case 46: o=t_f(o,0);break; // f // might need o=vo here
      case 47: dim();break;
      case 48: bload();break;
      case 49: each(o);break;
      case 50: stk_dec();break;
      case 51: stpow();break;
      case 52: st_sqr();break;
      case 53: st_sqrt();break;
      case 54: o=loopy(o);break; // regulare while/until loop
      case 55: list2str();break; // make a string out of a list
      case 56: findit(1);break;
      case 57: tellType();break;
      case 58: pmul();break;
      case 59: len2int();break;
      case 60: logic2(0);break; // bitand
      case 61: logic2(1);break; // bitor
      case 62: logic2(2);break; // bitnand
      case 63: logic2(3);break; // bitnor
      case 64: logic2(4);break; // bitxor
      case 65: logic2(5);break; // bitxnor
      case 66: logic2(6);break; // modulo is close enuf to a logic2
      case 67: bitnot(1);break;  // bitnot
      case 68: logic2(7);break;  // and
      case 69: logic2(8);break;  // or
      case 70: logic2(9);break;  // nand
      case 71: logic2(10);break; // nor
      case 72: logic2(11);break; // xor
      case 73: logic2(12);break; // xnor
      case 74: bitnot(0);break;  // not
      case 75: bin2int();break;  // "10010101" -> an integer
      case 76: int2bin();break;
      case 77: str2list();break;
      case 78: reverse();break;
      case 79: pushReal(M_PI);break;
      case 80: getUserInput();break;
      case 81: isEof();break;
      case 82: readLineFromTextFile();break;
      case 83: o=swcase(o);break;
      case 84: getIO(1);break; // reserve some ports
      case 85: getIO(0);break; // free them
      case 86: byteout();break;   // send data
      case 87: bytein();break;    // read data
      case 88: split();break;
      case 89: hep();break;       // show some help
      case 90: bitshift(1);break; // shl
      case 91: bitshift(0);break; // shr
      case 92: // "arg" and "args" are the same function
      case 93: stkArgs();break;  // complain if not enuf onstack
      case 94: seconds();break; // real# delay
      case 95: usrTimer();break; //real# timecalc since prog start
      case 96: real2int(1);break; //floor
      case 97: real2int(2);break; //round
      case 98: real2int(3);break; //ceil
      case 99: irnd();break; //random integer
      case 100: rnd();break; //random real #
      case 101: str2real();break; //val
      case 102: osCmd();break; // shell
      case 103: CD();break;
      case 104: requireType(anInt);break; // these could be used
      case 105: requireType(aReal);break; // by their cFunc numbers
      case 106: requireType(aStr);break;  // by another cFunc
      case 107: requireType(aList);break; // that would take a list
      case 108: requireType(anObj);break; // and check the stack
      case 109: requireType(aFile);break; // for types
      case 110: requireType(aNum);break; // ANY number, int or real
      case 111: evalStr();break;   // eval string (tokenize it first)
      case 112: bsave();break;
      case 113: fileSeek(1);break; // absolute fileseek
      case 114: frdInt();break;
      case 115: o=leg(o);break;
      case 116: askType(anInt);break;
      case 117: askType(aReal);break;
      case 118: askType(aStr);break;
      case 119: askType(aList);break;
      case 120: askType(anObj);break;
      case 121: askType(aFile);break;
      case 122: askType(aNum);break;
      case 123: listItems();break; // break open a list onto the stack
      case 124: add2(); break;
      case 125: uSubStr();break;
      case 126: slice();break;
      case 127: splice();break; // splices strings, lists, objects
      case 128: trig(1);break;
      case 129: trig(2);break;
      case 130: trig(3);break;
      case 131: trig(4);break;
      case 132: trig(5);break;
      case 133: trig(6);break;
      case 134: trigUnits=180/M_PI;break;
      case 135: trigUnits=1;break;
      case 136: trimS();break;
      case 137: findit(0);break;
      case 138: notZero();break; //nz
      case 139: chopS();break;
      case 140: filePos();break;
      case 141: fileSeek(0);break; // relative fileseek
      case 142: charsToFile();break;
      case 143: fwrInt();break;
      case 144: listSelect();break;
      case 145: endian=0;break;
      case 146: endian=1;break;
      case 147: mez("\"ls\" sys drop");break;
      case 148: mez("\"clear\" sys drop");break;
      case 149: o=floop(o);break; // for loop
      case 150: Here();break;//get current dir
      case 151: whileOrUntil=1;break;
      case 152: whileOrUntil=0;break;
      case 153: uNow();break;
      case 154: doInStkObj();break;
      case 155: err=2;puts("(Hit a 'STOP' command)");break;//stop
      case 156: breakout();return o;//"brk"
      case 157: putStrValBinary(); break;
      case 158: doFile(0); break;
      case 159: o=doBlk(o);break;
      case 160: mez("here >x home cd \"less yapi.txt\" sys drop");
                mez("<x cd");break; // help
      case 161: ind=1;break;//for indirect list indexinsg
      case 162: doToListItem(1);break; // +#
      case 163: doToListItem(2);break; // -#
      case 164: doToListItem(3);break; // *#
      case 165: doToListItem(4);break; // /#
      case 166: doToListItem(5);break; // &#
      case 167: doToListItem(6);break; // |#
      case 168: doToListItem(7);break; // ^#
      case 169: doToListItem(8);break; // %#
      case 170: incDecListItem(1);break; // ++#
      case 171: incDecListItem(-1);break; // --a#
      case 172: isItZero();break; // z
      case 173: putStr("listen not functional in this revision");break;//n startServer();break;
      case 174: putStr("hear not functional in this revision");break;//n getQuery();break;
      case 175: putStr("reply not functional in this revision");break;//n replyHTML();break;
      case 176: uMidStr();break;//pushInt(listeningTo);break;
      case 177: urlDec();break;
      case 178: putStr("mime not functional in this revision");break;//n setMimeStr();break;
      case 179: pushInt(neg(pop));break; //mem leak ?!?
      case 180: pushInt(pos(pop));break;
      case 181: allSplice();break;
      case 182: pushQs(homeDir);break;
      case 183: yapiInfo();break;
      case 184: natLog();break;
      case 185: uSort();break;
      default:
      if(listItemInt(Lp,IP)<0){ // a cFunc but don't run it
        V=int2val(-listItemInt(Lp,IP));
        V.vT=aCfunc;push(V);}
      else zbail("go: unrecognized cFunc in ");
    }
    if(LENIENT)STRICT=anyType;LENIENT=1; // timeOut for strict typing
    if(err<0)return o; //brk
    if(BAD){//an actual error occurred
      switch(listItemInt(Lp,IP)){
        case 104: case 105: case 106: case 107: case 108:
        case 109: case 110: case 92: case 93: case 124:
        printf(" \nWord %d in the cmd named ",IP);
        return o;
        default:
        if(err==1)//if >1, it's a stop.
          printf("\n(Error in %s)\nWord %d in ",cFuncs[listItemInt(Lp,IP)],IP);
        else
          printf(" %s\nWord %d in ",cFuncs[listItemInt(Lp,IP)],IP);
        return o;
      }
    }
    break;
    default: printf("go: unknown type %d\n",listItemType(Lp,IP));
  }
z:return o;}

val mkCfunc(char* s){ // used by detect
  val cf; int i;
  for(i=0;cFuncs[i];i++)//Look thru ALL the cFuncs listed
    if(strcmp(s,cFuncs[i])==0)//Found it
      {cf.vT=aCfunc; cf.vD.I=i; return cf;}//Return its number in a val
  retZ;// The calling Fn will know it wasn't found by its type
}

// ==== figure out what kind of value type this string
// ==== represents, and return it as a typed value
val detect(char* DS){
  char* DS1;// DS starting with the 2nd char
  val CF;   // the cFunc it might be
  int i;    // the integer it might be
  double r; // the real number it might be
  char E;   // the end-char of DS in case it's a binary or hex number
  int L;    // the length of DS
  int n;    // counting
  DS1=&DS[1];L=strlen(DS);
  CF=mkCfunc(DS);if(CF.vT==aCfunc)return(CF);
//if it got here, it's not a YAPI native function
  switch(DS[0]){
    case  39: case '=': case '<': case '>': case ':':
    case ',': case ';': case '%': case '@': case '~':
    case '?': case '.': case '$': case '^': case '!':
    case '*': case '/': case '&': case '|': case '`':
      return cmd2val(DS);//cmd
    case '-': case '+': 
      if(!strchr(".0123456789",DS[1]))return cmd2val(DS);
      break;
    case 'h': case 'H':
      for(n=1;n<L;n++)
        if(!strchr("0123456789abcdefABCDEF",DS[n]))goto z;
      if(sscanf(DS1,"%x",&i))return int2val(i);//hex
    case 'b': case 'B':
      i=0;
      for(n=1;n<L;n++)
        if(DS[n]=='1')i+=(1<<(L-n-1));//49 is "1"
        else if(DS[n]!='0')goto z;
      return(int2val(i));
    break;
  }
//if it got here, it's not a usrCmd or a binary or a hex
  if((sscanf(DS,"%lf",&r))&&(strlen(DS)>1)){//could be a real#
    if((memchr(DS,'.',strlen(DS)))//has dp
    ||(memchr(DS,'e',strlen(DS)))//has exponent
    ||(memchr(DS,'E',strlen(DS)))) return real2val(r);//has EXPONENT
  }
//if it got here, it's not a real number either
  if(sscanf(DS,"%d",&i))return int2val(i); // must be an integer
//if it got here, it aint an integer
z:puts("");
  putStr(DS);
  puts(" is not recognized as a valid command or data ");
  err=1;retZ;
//and if it was a QUOTED string (or a list?),
//it wouldn't've been sent to "Detect" in the first place...
};

short cmt = 0; // level of commenting (parentheses)

void pushToken(char* w){
  val v;
  if(cmt)return; // ignore anything inside a comment
  if(strcmp(w,"")==0)return; // ignore blank tokens
  v = detect(w);
  push(v); // even if v is an error, put it onstack anyway
}//pushToken might also be usable with "val"(str2num)

#define PTK pushToken(tKn);tKn[t=0]=0;
char quo = 0; // IS a quoted-string in progress?

void tokenize(char* tLine){ // MAYBE make this a user func also?
  char tKn[90];// holds a token to be processed by pushToken
  char c;       // holds 1 char out of the tLine
  int n;         // to traverse the tLine
  int t;          // to build the token
  tKn[t=quo=0]=0;  // init
  if(strlen(tLine)==0)return;       // ignore blank lines
  for(n=0; n<=strlen(tLine); n++){  // take it 1 char at a time
    c = tLine[n];                   // c .. that's the 1 char
    if(cmt)switch(c){ // in a comment so just keep track of parentheses level
      case '(': cmt++; break; // for if you have parentheses inside a comment
      case ')': cmt--; break;}// close parentheses (closes comment if zero)
//    only here in a comment or in a quoted-string should it ever find a ')'
//    and no default because stuff inside a comment is just ignored anyway
    else if(quo)switch(c){ // currently in a quoted-string so just build that
      case '"': pushQs(tKn);tKn[t=quo=0]=0;break;//end-quote & store
//    Note that with a string, it bypasses "pushToken" and "detect"
      default: tKn[t++]=c;tKn[t]=0;} // add 1 char to quoted-string
    else switch(c){ // NOT in a comment or a quote
      case '\\': pushInt(tLine[++n]);break; //  "\c"=char2int
      case '"': PTK; quo=1; break; // BEGIN a quoted-string
      case '[': PTK; brack++; pushMkr(aList); break; // start a list
      case ']':
        if(brack<1)bail("Can't close a list without opening one first ");
        PTK; brack--; pushListFromStk; break;   // end [] list
      case '}': // does nothing, just syntax-sugar.
      case '{': // Use it to make code look more understandable
      case ' ': PTK; break; // SPACE is what seperates the words
      case '(': PTK; cmt++; break; // START a comment
      case ')': bail("Can't close a comment w/o opening one first ");break;
//    if it found the ')' down here, it wasn't even IN a comment so just bail
      default: tKn[t++]=c; tKn[t]=0; // add 1 char to the token/word
    }
  }
//once it gets here, the entire line has been gone thru
  if(t>0)pushToken(tKn);// any remaining nonblank since no ' ' at end-of-line
  if(quo)gripe("Quote left open ");
}


usrFn doFile(int runit){ // load (and run?) a file
  FILE* F;      // handle to load
  string fn;    // file name
  int lnum;     // line number?
  val stuff;    // stuff to run when tokenized
  char fl[256]; // the line of text(raw)
  lnum=0;
  if(noStk(1)||noStr(1)){
    gripe("Need the name of a file to ");
    if(runit)putStr("run ");else putStr("load ");
    return;
  }
  fn=popStr;
  F=fopen(strS(fn),"r");//not keeping the filenameStr
  if(F==NULL){
    gripe("Can't find a file named ");
    putStrQ(strS(fn));
    puts(" to run. ");
    dropStr(fn);
    return;
  }
  dropStr(fn);
  pushMkr(aList);
  while(feof(F)==0){
    fl[0]=0; // zero it out in case fgets gets nothing
    fgets(fl,255,F);  lnum++;
    fl[strcspn(fl, "\r\n")] = 0; // ditch trailing newline
    tokenize(fl);
    if(BAD){
      printf("line #%d in the file %s",lnum,strS(fn));
      while(sp>0)dropVal(pop);
      fclose(F);return;
    }
  }
  fclose(F);
  stuff=list2val(mkListFromStk());
  if(runit){
    ogo(stuff,globs);//maybe should be go(stuf,vO) or just "do"
    dropVal(stuff);
  }else push(stuff);
}

void runArgs(char* s){pushQs(s);doFile(1);}

int main(int argc, char *argv[]){
  val sb; int n; char usrlin[90];
  endian=0; // little-endian;
  whileOrUntil=-1;
  initportsok();//for absolute port I/O
  getcwd(homeDir, sizeof(homeDir)); // remember where YAPI was run
  n=strlen(homeDir);homeDir[n]=homeDir[0];homeDir[n+1]=0; // tack / on end
  time_t rnd__seed;srand((unsigned) time(&rnd__seed));//for rnd
  strcpy(usrlin,"");
  maloc_init();
  initGlobs();
  if(argc>2){
    pushMkr(aList);
    for(n=2;n<argc;n++)pushQs(argv[n]);
    pushListFromStk;
    globs=uMkVar(globs,"cmd");
  }

  if(argc>1)runArgs(argv[1]);else{
    mez("\"clear\" sys drop");
    puts("YAPI (Yet Another Postfix Interpreter) (RPN)");
    puts("Build: 2024.DEC.20");
    puts("For a list of commands, type h");
    puts("See also yapi.txt");
  }

  while(strcmp(usrlin,"q")){
    if(brack<1)printf("\n(%d) ",sp);else printf("[ ");
    err=0;
    fflush(stdout);
    fgets(usrlin, sizeof(usrlin), stdin);
    usrlin[strcspn(usrlin, "\r\n")] = 0; // ditch the CRs and the LFs
    if(strcmp(usrlin,"q")==0){
      while(sp>0)dropVal(pop); // clear stack
      dbg('Z');
      killObj(globs);
      dot;
      if(allocated!=0){puts("\nMemory Leak:");showMem();};
      freeallports();
//n   if(listeningTo)close(servSock);//network
      exit(0);}
    if(brack<1)pushMkr(aList);
    tokenize(usrlin);
    if(BAD){
      putStr("in the line: ");
      puts(usrlin);
      putStr("( type h to get a list of valid built-in commands )");
      while(sp>0)dropVal(pop);}
    if((brack<1)&&(OK)){
      sb = list2val(mkListFromStk());
      ogo(sb,globs);
      if(BAD){
        putStr("the line: ");
        putStr(usrlin);}
      dropVal(sb);
    }
  }
}