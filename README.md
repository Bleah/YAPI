# YAPI

Yet Another Postfix Interpreter (only tried on linux so far)<BR/>
It's like Reverse Polish Notation where when you call a function you put the parameters first, as in:<br/>
"filename.ext" \r file >variablename<br/><br/>
Some documentation is in the "help" folder<BR/>
Some functions might get changed later<Br/>
NOTE: This interpreter uses a custom allocator,
and it has limitations on how many things it can allocate.
Hopefully, that limitation will be gone soon.<BR/><br/>
Stuff it does:<BR/>
math stuff like add, subtract, multiply, divide, etc, and ..some.. vector stuff<br/>
Logic stuff like boolean and bitwise (and, or, nand, nor, xor, etc...)<BR/>
File operations like read/write lines, strings; also binary-file stuff like seek, read/write specified-length strings and binary-coded integers.  big-endian/little-endian can be selected.<BR/>
Some trig functions.<br/>
Counted loops, do-loops, while loops, until loops, for loops...<BR/>
"if" type comparisons, although they're not called "if" here..<BR/>
User-defined functions. (supports recursion)<br/>
Integers, reals, strings, lists, file-handles, objects...<br/>
Absolute-port-number I/O, provided that you have a card installed that can take port numbers, such as those old-style parallel printer ports that showed up at port 0x378 hex<Br/>
Load/run/include scripts [*.pf] <br/>
Send commands to system (bash) <br/><br/>
Stuff it doesn't do:<br/>
Serial(async) I/O, network I/O, GUI/graphics, or the ability to use a precompiled shared libaray(.dll, .so)<br/>
(Maybe someday, if I can ever figure out HOW to get it to do those things...)